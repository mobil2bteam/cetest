//
//  Helper.h
//  DrinkAndDrive
//
//  Created by Ruslan on 1/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#ifndef Header_h
#define Header_h
#import "Constants.h"

// categories
#import "UIColor+RPHexadecimal.h"
#import "UIButton+BackgroundColor.h"
#import "UIImage+CropImage.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIViewController+RPShowAlert.h"
#import "UIViewController+RPReachability.h"
#import "NSString+Category.h"
#import "UIImageView+AFNetworking.h"

// libs
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <VKSdk.h>
#import "ECSlidingViewController.h"
#import "MBProgressHUD.h"
#import <AFNetworking.h>
#import <GBDeviceInfo/GBDeviceInfo.h>
#import "SSZipArchive.h"
#import <MagicalRecord/MagicalRecord.h>

//server manager
#import "RPServerManager.h"
#import "AppDelegate.h"

//views
#import "RPCustomView.h"
#import "RPResultButton.h"

#import "RPOptions.h"
#import "UIView+Shake.h"
#import "UIImageView+ImageURL.h"

#endif /* Header_h */

