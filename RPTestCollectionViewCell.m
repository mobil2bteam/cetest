//
//  RPTestCollectionViewCell.m
//  Cetest
//
//  Created by Ruslan on 3/11/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIColor+RPHexadecimal.h"
#import "RPTestCollectionViewCell.h"
#import "RPTest.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"

@implementation RPTestCollectionViewCell

- (void)configureCellWithTest:(NSArray <NSArray <RPTest*> *> *)data forIndexPath:(NSIndexPath*)indexPath{
    RPTest *currentTest = data[indexPath.section][indexPath.row];
    if (currentTest.buy_id) {
        self.priceLabel.text = @"Куплено";
    } else {
        if (currentTest.free_date_day) {
            if (currentTest.free_date_day == 1) {
                self.priceLabel.text = [NSString stringWithFormat:@"%ld руб./%ld день", (long)currentTest.price, (long)currentTest.free_date_day];
            } else {
                self.priceLabel.text = [NSString stringWithFormat:@"%ld руб./%ld дня", (long)currentTest.price, (long)currentTest.free_date_day];
            }
        } else {
            self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)currentTest.price];
        }
    }
    self.priceLabelHeightConstraint.constant = 21;
    [self customizeCellWithTest:currentTest];
}

- (void)configureCellWithUserTest:(NSArray <NSArray <RPTest*> *> *)data forIndexPath:(NSIndexPath*)indexPath{
    RPTest *currentTest = data[indexPath.section][indexPath.row];
    self.priceLabel.text = @"";
    self.priceLabelHeightConstraint.constant = 0;
    [self customizeCellWithTest:currentTest];
}

- (void)configureCellWithFreeTest:(NSArray <RPTest*> *)data forIndexPath:(NSIndexPath*)indexPath{
    RPTest *currentTest = data[indexPath.row];
    self.priceLabel.text = @"";
    self.priceLabelHeightConstraint.constant = 0;
    [self customizeCellWithTest:currentTest];
}

- (void)customizeCellWithTest:(RPTest *)currentTest{
    self.layer.borderColor = [UIColor colorWithHexString:currentTest.discipline_color].CGColor;
    self.classLabel.textColor = [UIColor colorWithHexString:currentTest.discipline_color];
    self.testNameLabel.textColor = [UIColor colorWithHexString:currentTest.discipline_color];
    
    
    self.classLabel.text = currentTest.class_name;
    self.testNameLabel.text = currentTest.test_name;
    
//    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", kAddressSite, currentTest.image]];
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", @"http://eclass.cetest.ru/sc/", currentTest.image]];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.backgroundImage setImageWithURLRequest:imageRequest
                              placeholderImage:nil                                          success:nil
                                       failure:nil];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.layer.masksToBounds = NO;
    self.layer.borderWidth = 1.f;
}

- (NSInteger)calculateHeightForLbl:(NSString*)text width:(CGFloat)width{
    CGSize constraint = CGSizeMake(width, MAXFLOAT);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [text boundingRectWithSize:constraint
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}
                                            context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return (NSInteger)size.height;
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.priceLabel.text = @"";
    self.backgroundImage.image = nil;
    self.testImageView.image = nil;
    self.testNameLabel.text = @"";
}
@end
