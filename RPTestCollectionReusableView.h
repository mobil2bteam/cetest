//
//  RPTestCollectionReusableView.h
//  Cetest
//
//  Created by Ruslan on 3/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPTestCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *classLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyAllTestButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *classLabelBottomMarginConstraint;
@end
