//
//  Test+CoreDataProperties.m
//  Cetest
//
//  Created by Ruslan on 4/18/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Test+CoreDataProperties.h"

@implementation Test (CoreDataProperties)

@dynamic classID;
@dynamic filePath;
@dynamic name;
@dynamic testID;
@dynamic themeID;
@dynamic login;
@dynamic userID;

@end
