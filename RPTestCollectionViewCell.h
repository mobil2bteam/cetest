//
//  RPTestCollectionViewCell.h
//  Cetest
//
//  Created by Ruslan on 3/11/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPTest;

@interface RPTestCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UILabel *testNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *classLabel;

@property (weak, nonatomic) IBOutlet UIImageView *testImageView;

@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *testNameLabelHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *priceLabelHeightConstraint;

- (void)configureCellWithTest:(NSArray <NSArray <RPTest*> *> *)data forIndexPath:(NSIndexPath*)indexPath;

- (void)configureCellWithUserTest:(NSArray <NSArray <RPTest*> *> *)data forIndexPath:(NSIndexPath*)indexPath;

- (void)configureCellWithFreeTest:(NSArray <RPTest*> *)data forIndexPath:(NSIndexPath*)indexPath;

@end
