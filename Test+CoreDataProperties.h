//
//  Test+CoreDataProperties.h
//  Cetest
//
//  Created by Ruslan on 4/18/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Test.h"

NS_ASSUME_NONNULL_BEGIN

@interface Test (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *classID;
@property (nullable, nonatomic, retain) NSString *filePath;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *testID;
@property (nullable, nonatomic, retain) NSNumber *themeID;
@property (nullable, nonatomic, retain) NSString *login;
@property (nullable, nonatomic, retain) NSNumber *userID;

@end

NS_ASSUME_NONNULL_END
