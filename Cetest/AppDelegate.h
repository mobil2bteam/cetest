//
//  AppDelegate.h
//  Cetest
//
//  Created by Ruslan on 3/3/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPTest;
@class RPOptions;
@class RPClass;
@class RPUserInfo;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NSArray<RPClass *> *class_list;

@property (nonatomic, strong) NSArray <NSArray <RPTest *> *> *buyTestArray;

@property (nonatomic, strong) NSArray <RPTest*> *freeTestArray;

@property (nonatomic, strong) NSArray <NSArray <RPTest*> *> *allTestArray;

@property (nonatomic, strong) RPOptions *options;

@property (nonatomic, strong) RPUserInfo *user_info;

@property (nonatomic, strong) NSMutableArray <UINavigationController *> *navControllers;

@end

