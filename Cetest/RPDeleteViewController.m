//
//  RPDeleteViewController.m
//  Cetest
//
//  Created by Ruslan on 5/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPDeleteViewController.h"

@implementation RPDeleteViewController

#pragma mark - Actions

- (IBAction)deleteButtonPressed:(id)sender{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf.delegate deleteTest];
    }];
}

- (IBAction)cancelButtonPressed:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
