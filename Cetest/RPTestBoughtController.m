//
//  RPTestBoughtController.m
//  Cetest
//
//  Created by Ruslan on 3/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPTestBoughtController.h"
#import "RPFullTest.h"
#import "Constants.h"
#import "Header.h"

@interface RPTestBoughtController()

@property (weak, nonatomic) IBOutlet UIImageView *testImageView;

@property (weak, nonatomic) IBOutlet UILabel *classLabel;

@property (weak, nonatomic) IBOutlet UILabel *testLabel;

@property (weak, nonatomic) IBOutlet UIButton *returnButton;

@property (weak, nonatomic) IBOutlet UIView *cornerView;

@property (weak, nonatomic) IBOutlet UIView *shareView;

@end

@implementation RPTestBoughtController

- (void)viewDidLoad{
    [super viewDidLoad];
    [self prepareView];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    __weak typeof(self) weakSelf = self;
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         [weakSelf viewWillLayoutSubviews];

     } completion:nil];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.cornerView.layer.masksToBounds = YES;
    self.cornerView.layer.cornerRadius = CGRectGetHeight(self.cornerView.frame)/2;
    self.testImageView.layer.masksToBounds = YES;
    self.testImageView.layer.cornerRadius = CGRectGetHeight(self.testImageView.frame)/2;
    self.returnButton.layer.masksToBounds = YES;
    self.returnButton.layer.cornerRadius = 4;
    self.returnButton.layer.borderWidth = 1.f;
    self.returnButton.layer.borderColor = [UIColor colorWithRed:0.29 green:0.35 blue:0.36 alpha:1].CGColor;
}

- (void)prepareView{
    self.classLabel.text = self.currentTest.info.class_name;
    self.testLabel.text = self.currentTest.info.discipline_name;
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://eclass.cetest.ru/sc/%@", self.currentTest.info.discipline_img_url]];
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:url
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self.testImageView setImageWithURLRequest:imageRequest
                              placeholderImage:nil
                                       success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                           self.testImageView.image = [image cropToSquare];
                                       }failure:nil];
}

@end
