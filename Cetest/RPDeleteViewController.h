//
//  RPDeleteViewController.h
//  Cetest
//
//  Created by Ruslan on 5/12/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPThemeController.h"

@interface RPDeleteViewController : UIViewController

@property (nonatomic, strong) id <RPDeleteTestProtocol> delegate;

@end
