//
//  UIImage+CropImage.m
//  Cetest
//
//  Created by Ruslan on 3/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIImage+CropImage.h"

@implementation UIImage (CropImage)

- (UIImage *)cropToSquare{
    double refWidth = CGImageGetWidth(self.CGImage);
    double refHeight = CGImageGetHeight(self.CGImage);
    if (refWidth == refHeight) {
        return self;
    }
    CGFloat sideLength;
    CGFloat x, y;
    if (refWidth > refHeight) {
        sideLength = refHeight;
        x = (refWidth - refHeight)/2;
        y = 0;
    } else {
        sideLength = refWidth;
        y = (refHeight - refWidth)/2;
        x = 0;
    }
    CGRect cropRect = CGRectMake(x, y, sideLength, sideLength);
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], cropRect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:self.imageOrientation];
    CGImageRelease(imageRef);
    return cropped;
}

@end
