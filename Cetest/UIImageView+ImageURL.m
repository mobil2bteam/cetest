//
//  UIImageView+ImageURL.m
//  WorldOfTheFeed
//
//  Created by Ruslan on 2/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIImageView+ImageURL.h"
#import "UIImageView+AFNetworking.h"

@implementation UIImageView (ImageURL)

- (void)RP_setImageWithURL:(NSString *)url{
    
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString: url]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self setImageWithURLRequest:imageRequest
                              placeholderImage:[UIImage imageNamed:kPlaceholderImage]
                                       success:nil
                                       failure:nil];

}

@end
