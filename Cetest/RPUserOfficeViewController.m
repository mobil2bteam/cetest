//
//  RPUserOfficeViewController.m
//  Cetest
//
//  Created by Ruslan on 4/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPUserOfficeViewController.h"
#import "RPUserSendTableViewCell.h"
#import "Header.h"
#import "Constants.h"
#import "RPLogInVC.h"
#import "RPLogOutVC.h"
#import "RPUserInfo.h"

#define kRowHeight 60.f
#define kPasswordViewHeight 44.f

@interface RPUserOfficeViewController () <UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *userEmailTextField;

@property (weak, nonatomic) IBOutlet UITextField *sendNameTextField;

@property (weak, nonatomic) IBOutlet UITableView *sendListTableView;

@property (weak, nonatomic) IBOutlet UITextField *sendEmailTextField;

@property (nonatomic, strong) NSArray<Send_List *> *sendList;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userViewZeroHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addSendZeroHeightConstraint;

@property (weak, nonatomic) IBOutlet RPCustomView *sendView;

@property (weak, nonatomic) IBOutlet UIView *sendListView;

@property (weak, nonatomic) IBOutlet UIView *emptySendListView;

@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;

@end

@implementation RPUserOfficeViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Личный кабинет";
    self.sendListTableView.allowsSelection = NO;
    if (!kUser.isLogin) {
        RPLogInVC *vc = [[RPLogInVC alloc]initWithNibName:@"RPLogInVC" bundle:nil];
        [self.navigationController pushViewController:vc animated:NO];
    }
    self.sendListView.layer.masksToBounds = NO;
    self.sendListView.layer.shadowOffset = CGSizeMake(0, 0);
    self.sendListView.layer.shadowRadius = 4;
    self.sendListView.layer.shadowOpacity = 0.5;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
    if (kUser.isLogin) {
        self.changePasswordButton.hidden = kUser.socialNetwork != SocialTypeEmail;
        self.sendList = APP_DELEGATE.user_info.send_list;
        [self prepareView];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
        // скрываем надпись о рассылках
        self.emptySendListView.hidden = self.sendList.count;
        self.sendListView.hidden = NO;
    }
}

#pragma mark - Methods

- (void)prepareView{
    self.sendListView.hidden = YES;
    self.userNameTextField.text = kUser.name;
    self.userEmailTextField.text = kUser.email;
    [self.userImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kUser.photoUrl]] placeholderImage:[UIImage imageNamed:@"user"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
        self.userImageView.image = [image cropToSquare];
    } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
    }];
}

- (void)loadSendList{
    self.sendList = nil;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postGetSendListOnSuccess:^(RPSendList *send_list, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self.sendListTableView reloadData];
            [self showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                [self loadSendList];
            }];
        } else {
            self.sendList = send_list.send_list;
            [self.sendListTableView reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view layoutIfNeeded];
            });
            // скрываем надпись о рассылках
            self.emptySendListView.hidden = self.sendList.count;
            self.sendListView.hidden = NO;
        }
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.sendListTableView reloadData];
        [self showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [self loadSendList];
        }];
    }];
}

#pragma mark - Actions

- (IBAction)changePasswordButtonPressed:(id)sender {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Сменить пароль"
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Старый пароль";
         textField.secureTextEntry = YES;
     }];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Новый пароль";
         textField.secureTextEntry = YES;
     }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                               actionWithTitle:@"Отменить"
                               style:UIAlertActionStyleDefault
                                   handler:nil];
    
    UIAlertAction *saveAction = [UIAlertAction
                                   actionWithTitle:@"Сохранить"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       UITextField *oldPassword = alertController.textFields.firstObject;
                                       UITextField *newPassword = alertController.textFields.lastObject;
                                       if (!oldPassword.text.length || !newPassword.text.length) {
                                           [self showMessage:kWarningMessage];
                                       } else {
                                           [self changePasswordWithOldPassword:oldPassword.text newPaswrod:newPassword.text];
                                       }
                                   }];

    [alertController addAction:cancelAction];
    [alertController addAction:saveAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)changePasswordWithOldPassword:(NSString *)oldPassword newPaswrod:(NSString *)newPassword{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postChangePasswordWithOldPassword:[[oldPassword md5]md5] newPassword:[[newPassword md5]md5] onSuccess:^(RPError *error, RPUserInfo *user_info) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self showMessage:error.msg];
        } else {
            [self showMessage:@"Пароль успешно изменен!"];
            [kUserDefaults setValue:user_info.password forKey:@"password"];
            [kUserDefaults synchronize];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:kErrorMessage];
    }];
}

- (IBAction)deleteSendButtonPressed:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postRemoveFromSendListWithID:self.sendList[sender.tag].ID onSuccess:^(RPSendList *send_list, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self showMessage:error.msg];
        } else {
            [self loadSendList];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:kErrorMessage];
    }];
}

- (IBAction)sendSwitchChanged:(UISwitch *)sender {
    Send_List *send = self.sendList[sender.tag];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postEditSendWithName:send.name email:send.email ID:send.ID active:sender.isOn onSuccess:^(RPSendList *send_list, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self showMessage:error.msg];
        } else {
            [self loadSendList];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:kErrorMessage];
    }];
}

- (IBAction)exitButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    RPLogOutVC *restorePasswordVC = [[RPLogOutVC alloc]initWithNibName:@"RPLogOutVC" bundle:nil];
    restorePasswordVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    restorePasswordVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    restorePasswordVC.logOutCallback  = ^{
        [RPUser logOut];
        RPLogInVC *vc = [[RPLogInVC alloc]initWithNibName:@"RPLogInVC" bundle:nil];
        [weakSelf.navigationController pushViewController:vc animated:NO];
        for (UINavigationController *navVC in APP_DELEGATE.navControllers) {
            [navVC popToRootViewControllerAnimated:NO];
        }
    };
    [self presentViewController:restorePasswordVC animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.sendList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPUserSendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPUserSendTableViewCell class]) forIndexPath:indexPath];
    Send_List *send = self.sendList[indexPath.row];
    cell.emailLabel.text = send.email;
    cell.nameLabel.text = send.name;
    cell.deleteSendButton.tag = indexPath.row;
    cell.sendSwitch.on = send.active;
    cell.sendSwitch.tag = indexPath.row;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.f;
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)addNewSendButtonPressed:(id)sender {
    [self showAddSendView:YES];
}

- (IBAction)hideSendViewButtonPressed:(id)sender {
    [self showAddSendView:NO];
}

- (void)showAddSendView:(BOOL)show{
    self.addSendZeroHeightConstraint.priority = show ? 250 : 900;
    self.userViewZeroHeightConstraint.priority = show ? 900 : 250;
    [UIView animateWithDuration:0.1 animations:^{
        [self.view layoutIfNeeded];
    }];
    [self.sendView setNeedsDisplay];
}

- (IBAction)addSendButtonPressed:(id)sender {
    if (!self.sendNameTextField.text.length || !self.sendEmailTextField.text.length) {
        [self showMessage:@"Заполните необходимые поля"];
    } else {
        [self showAddSendView:NO];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[RPServerManager sharedManager]postAddToSendListWithName:self.sendNameTextField.text email:self.sendEmailTextField.text onSuccess:^(RPSendList *send_list, RPError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (error) {
                [self showMessage:error.msg];
            } else {
                [self loadSendList];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self showMessage:kErrorMessage];
        }];
    }
}
@end
