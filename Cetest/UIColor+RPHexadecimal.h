//
//  UIViewController+RPHexadecimal.h
//  Cetest
//
//  Created by Ruslan on 3/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RPHexadecimal)

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
