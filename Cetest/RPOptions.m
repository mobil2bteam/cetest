//
//  RPOptions.m
//  Е-класс
//
//  Created by Ruslan on 3/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPOptions.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "RPTest.h"
#import "RPClassList.h"

@implementation RPOptions

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPClass class] forKeyPath:@"class_list" forProperty:@"class_list"];
        [mapping hasMany:[RPTest class] forKeyPath:@"discipline_list" forProperty:@"discipline_list"];
        [mapping hasMany:[RPTest class] forKeyPath:@"test_list" forProperty:@"test_list"];
        [mapping hasMany:[RPTest class] forKeyPath:@"user_test_list.buy_test_list" forProperty:@"buy_test_list"];
        [mapping hasMany:[RPTest class] forKeyPath:@"user_test_list.free_test_list" forProperty:@"free_test_list"];

    }];
}

@end


//@implementation RPClass
//
//+(EKObjectMapping *)objectMapping
//{
//    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
//        [mapping hasMany:[RPClass class] forKeyPath:@"class_list"];
//    }];
//}
//
//+ (NSInteger)priceForClass:(NSString *)className {
//    for (RPClass *class in APP_DELEGATE.options.class_list) {
//        if ([class.name isEqualToString:className]) {
//            return class.price;
//        }
//    }
//    return 0;
//}
//
//+ (NSString *)productKeyForClassWithID:(NSInteger)ID {
//    for (RPClass *class in APP_DELEGATE.options.class_list) {
//        if (class.ID == ID) {
//            return class.key_ios;
//        }
//    }
//    return @"";
//}
//
//@end
//
//
//@implementation RPTest
//
//+(EKObjectMapping *)objectMapping
//{
//    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
//        [mapping mapPropertiesFromArray:@[@"buy_id",
//                                          @"class_id",
//                                          @"class_name",
//                                          @"discipline_color",
//                                          @"discipline_font_color2",
//                                          @"discipline_font_color1",
//                                          @"discipline_icon_url",
//                                          @"discipline_id",
//                                          @"discipline_img_url",
//                                          @"discipline_name",
//                                          @"key_ios",
//                                          @"free_date_day",
//                                          @"test_name",
//                                          @"image",
//                                          @"price"]];
//        [mapping mapKeyPath:@"id" toProperty:@"ID"];
//    }];
//}
//
//@end

