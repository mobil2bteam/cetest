//
//  RPThemeInfo.m
//  Cetest
//
//  Created by Ruslan on 3/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPThemeInfo.h"

@implementation RPThemeInfo

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[Info class] forKeyPath:@"theme_info.info" forProperty:@"info"];
        [mapping hasOne:[Options class] forKeyPath:@"theme_info.options" forProperty:@"options"];
        [mapping hasOne:[Subject class] forKeyPath:@"theme_info.subject" forProperty:@"subject"];
    }];
}

@end


@implementation Subject

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"tp",
                                          @"procent",
                                          @"url_web",
                                          @"url_file",
                                          @"sp",
                                          @"result_key",
                                          @"discription"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

- (NSString *)description{
    NSString *result = [NSString stringWithFormat:@"name - %@m tp - %ld, procent - %ld, sp - %ld, descpription - %@", self.name, (long)self.tp, (long)self.procent, (long)self.sp, self.discription];
    return result;
    
}


@end


