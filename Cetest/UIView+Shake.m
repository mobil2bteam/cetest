//
//  UIView+Shake.m
//  Е-класс
//
//  Created by Ruslan on 3/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIView+Shake.h"

#import "UIView+Shake.h"

@implementation UIView (Shake)

- (void)shake{
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:2];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([self center].x - 20.0f, [self center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([self center].x + 20.0f, [self center].y)]];
    [[self layer] addAnimation:animation forKey:@"position"];
}

@end
