//
//  RPUserInfo.m
//  Cetest
//
//  Created by Ruslan on 3/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPUserInfo.h"
#import "RPUser.h"
@implementation RPUserInfo

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"class_id",
                                          @"vk_id",
                                          @"name_full",
                                          @"password",
                                          @"fb_id",
                                          @"email"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
        [mapping hasMany:[Send_List class] forKeyPath:@"send_list"];
        [mapping hasMany:[ResultServerModel class] forKeyPath:@"result_list" forProperty:@"last_result_list"];
        [mapping hasMany:[ResultServerModel class] forKeyPath:@"all_result_list" forProperty:@"result_list"];
    }];
}

- (NSArray *)groupedResultsByClasses{
    // если результатов нет, то возвращаем nil
    if (!self.result_list.count) {
        return nil;
    }
    NSMutableArray <NSArray <ResultServerModel *> *> *groupedResults = [[NSMutableArray alloc]init];
    
    // получаем массив названий классов
    NSArray *classNames = [self.result_list valueForKeyPath:@"@distinctUnionOfObjects.class_name"];
    
    // сортируем массив названий классов
    classNames = [classNames sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (NSString *name in classNames) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_name = %@", name];
        
        // получаем массив тестов для данного класса
        NSArray *classes = [self.result_list filteredArrayUsingPredicate:predicate];
        
        // получаем массив названий тестов
        NSArray *testNames = [classes valueForKeyPath:@"@distinctUnionOfObjects.test_name"];
        NSMutableArray *tests = [[NSMutableArray alloc]init];
        // группируем по предметам
        for (NSString *test in testNames) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(class_name = %@) AND (test_name = %@)", name, test];
            NSArray *test = [self.result_list filteredArrayUsingPredicate:predicate];
            
            
            [tests addObject:test];
        }
        [groupedResults addObject:tests];
    }
    return [groupedResults copy];
}

//@property (strong, nonatomic) NSArray <NSArray < NSArray <ResultServerModel *> *> *> *filteredResults;
//                                       класс     тест
- (NSArray *)resultsByClass:(NSString *)className andTest:(NSString *)testName{
    if (!self.result_list.count) {
        return nil;
    }
    for (NSArray < NSArray <ResultServerModel *> *> *array in [self groupedResultsByClasses]) {
        for (NSArray <ResultServerModel *> *array2 in array) {
            if ([array2[0].test_name isEqualToString:testName] && [array2[0].class_name isEqualToString:className]) {
                return array2;
            }
        }
    }
    return nil;
}

- (NSArray *)resultsByClass:(NSString *)className andTest:(NSString *)testName andTheme:(NSString *)themeName{
    if (!self.result_list.count) {
        return nil;
    }
    NSMutableArray <ResultServerModel *> *results = [[NSMutableArray alloc]init];
    for (NSArray < NSArray <ResultServerModel *> *> *array in [self groupedResultsByClasses]) {
        for (NSArray <ResultServerModel *> *array2 in array) {
            for (ResultServerModel *result in array2) {
                if ([result.test_name isEqualToString:testName] && [result.class_name isEqualToString:className] && [result.theme_name isEqualToString:themeName]) {
                    [results addObject:result];
                }
            }
        }
    }
    return [results copy];
}

@end

@implementation Send_List

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"user_id",
                                          @"active",
                                          @"name",
                                          @"date",
                                          @"email"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end


@implementation ResultServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"procent",
                                          @"tp",
                                          @"sp",
                                          @"ps",
                                          @"class_id",
                                          @"test_id",
                                          @"theme_id",
                                          @"class_name",
                                          @"test_name",
                                          @"theme_name",
                                          @"date_reg"]];
    }];
}

@end


