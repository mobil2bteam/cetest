//
//  RPResultStep2ViewController+UICollectionView.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPResultStep2ViewController+UICollectionView.h"
#import "RPResultHeaderCollectionViewCell.h"
#import "RPResultCell.h"
#import "RPUserInfo.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "RPStatisticTestVC.h"

@implementation RPResultStep2ViewController (UICollectionView)


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.filteredTests.count;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.resultsCollectionView.bounds), 50);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPResultCell *cell = (RPResultCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPResultCell class]) forIndexPath:indexPath];
    cell.resultLabel.text = self.filteredTests[indexPath.row].theme_name;
    CGFloat ball = [self ballForIndexPath:indexPath];
    cell.ballLabel.text = [NSString stringWithFormat:@"%1.f", ball];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPResultHeaderCollectionViewCell *headerView = (RPResultHeaderCollectionViewCell*)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPResultHeaderCollectionViewCell class]) forIndexPath:indexPath];
        headerView.headerLabel.text = [NSString stringWithFormat:@"%@ (%@)",self.filteredTests[0].test_name, self.filteredTests[0].class_name];
        __weak typeof(self) weakSelf = self;
        headerView.callback = ^(NSInteger section){
            [weakSelf goToTestWithIndexPath:section];
        };
        reusableview = headerView;
    }
    return reusableview;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 50);
}

- (CGFloat)ballForIndexPath:(NSIndexPath *)indexPath{
    ResultServerModel *selectedResult = self.filteredTests[indexPath.row];
    for (ResultServerModel *result in APP_DELEGATE.user_info.last_result_list) {
        if ([result.class_name isEqualToString:selectedResult.class_name]) {
            if ([result.test_name isEqualToString:selectedResult.test_name]) {
                if ([result.theme_name isEqualToString:selectedResult.theme_name]) {
                    return result.sp;
                }
            }
        }
    }
    return 0;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ResultServerModel *selectedResult = self.filteredTests[indexPath.row];
    NSMutableArray <ResultServerModel *> *filteredArray = [[NSMutableArray alloc]init];
    for (ResultServerModel *result in APP_DELEGATE.user_info.result_list) {
        if ([result.class_name isEqualToString:selectedResult.class_name]) {
            if ([result.test_name isEqualToString:selectedResult.test_name]) {
                if ([result.theme_name isEqualToString:selectedResult.theme_name]) {
                    [filteredArray addObject:result];
                }
            }
        }
    }
    
    RPStatisticTestVC *vc = [[UIStoryboard storyboardWithName:@"Result" bundle:nil]instantiateViewControllerWithIdentifier:@"RPStatisticTestVC"];
    vc.resultsArray = [filteredArray copy];
    vc.isTest = NO;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)goToTestWithIndexPath:(NSInteger)section{
    ResultServerModel *selectedResult = self.filteredTests[0];
    NSMutableArray <ResultServerModel *> *filteredArray = [[NSMutableArray alloc]init];
    for (ResultServerModel *result in APP_DELEGATE.user_info.result_list) {
        if ([result.class_name isEqualToString:selectedResult.class_name]) {
            if ([result.test_name isEqualToString:selectedResult.test_name]) {
                    [filteredArray addObject:result];
            }
        }
    }
    
    RPStatisticTestVC *vc = [[UIStoryboard storyboardWithName:@"Result" bundle:nil]instantiateViewControllerWithIdentifier:@"RPStatisticTestVC"];
    vc.resultsArray = [filteredArray copy];
    vc.isTest = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
