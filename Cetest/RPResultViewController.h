//
//  RPResultViewController.h
//  Е-класс
//
//  Created by Ruslan on 4/22/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ResultServerModel;

@interface RPResultViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *resultsCollectionView;

@property (strong, nonatomic) NSArray <NSArray < NSArray <ResultServerModel *> *> *> *filteredResults;

- (void)showTestWithIndexPath:(NSIndexPath *)indexPath;

@end
