//
//  RPThemeController.h
//  Cetest
//
//  Created by Ruslan on 3/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Theme_List;

@protocol RPShareResultProtocol <NSObject>

- (void)prepareShareViaVK;

- (void)prepareShareViaFB;

@end

@protocol RPDeleteTestProtocol <NSObject>

- (void)deleteTest;

@end


@interface RPThemeController : UIViewController<RPShareResultProtocol>

@property (nonatomic, strong)  Theme_List *currentTheme;

@end

