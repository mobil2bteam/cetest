//
//  RPShowTestController.h
//  Cetest
//
//  Created by Ruslan on 4/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Test;

@interface RPRunTestController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, assign) BOOL isLocalLink;

@property (nonatomic, strong) NSString *link;

@property (nonatomic, strong) Test *localTest;

@property (nonatomic, assign) NSInteger themeID;
@end
