//
//  RPSigInViewController.m
//  Е-класс
//
//  Created by Ruslan on 9/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSigInViewController.h"

@interface RPSigInViewController ()

@end

@implementation RPSigInViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)tapGesture{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)registrationButtonPressed:(id)sender{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf.delegate registration];
    }];
}

- (IBAction)authorizationButtonPressed:(id)sender{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        [weakSelf.delegate authorization];
    }];
}


@end
