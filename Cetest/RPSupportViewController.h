//
//  RPSupportViewController.h
//  Е-класс
//
//  Created by Ruslan on 4/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPSupportViewController : UIViewController

@property (assign, nonatomic) BOOL isFeedback;

@property (assign, nonatomic) NSInteger rating;

@end
