//
//  RPSignInVC.m
//  Е-класс
//
//  Created by Ruslan on 3/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPSignInVC.h"
#import "Header.h"
@import FirebaseInstanceID;

@interface RPSignInVC ()

@property (weak, nonatomic) IBOutlet UIView *nameView;

@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *passwordView;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UISwitch *userInfoSwitch;

@end

@implementation RPSignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isSocial) {
        [self.passwordView removeFromSuperview];
        [self.registerButton setTitle:@"ПОДТВЕРДИТЬ" forState:UIControlStateNormal];
        self.emailTextField.text = kUser.email;
        self.nameTextField.text = kUser.name;
    }
    self.title = @"Регистрация";
    for (UIView *view in @[self.emailView, self.passwordView, self.nameView]) {
        view.layer.borderColor = [UIColor grayColor].CGColor;
        view.layer.borderWidth = 1.f;
        view.layer.masksToBounds = YES;
    }
    
    self.registerButton.layer.borderWidth = 1.f;
    self.registerButton.layer.cornerRadius = 10.f;
    self.registerButton.layer.borderColor = [UIColor grayColor].CGColor;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self.navigationItem setHidesBackButton:NO];
}

- (IBAction)registerButtonPressed:(id)sender {
    if (self.nameTextField.text.length == 0) {
        [self.nameView shake];
        return;
    }
    if (self.emailTextField.text.length == 0) {
        [self.emailView shake];
        return;
    }
    if (self.passwordTextField.text.length == 0 && !self.isSocial) {
        [self.passwordView shake];
        return;
    }
    if (!self.userInfoSwitch.isOn) {
        [self showMessage:@"Разрешите использовать ваши данные"];
        return;
    }
    [self registration];
}

- (void)registration{
    NSString *model = [GBDeviceInfo deviceInfo].modelString;
    NSString *token = @"token";
    if ([[FIRInstanceID instanceID] token] != nil) {
        token = [[FIRInstanceID instanceID] token];
    }
    NSMutableDictionary *params = [@{@"type":@"new",
                                     @"imei":[RPUser imei],
                                     @"gcm_id":token,
                                     @"type_phone":model
                                     } mutableCopy];
    if (self.isSocial) {
        params[@"name_full"] = kUser.name;
        params[@"email"] = kUser.email;
        params[@"login"] = kUser.userId;
        params[@"password"] = [[[NSString stringWithFormat:@"%@%@",kUser.userId, [kUser.userId reverseString]]md5]md5];
        if (kUser.socialNetwork == SocialTypeVK) {
            params[@"vk_id"] = kUser.userId;
        } else {
            params[@"fb_id"] = kUser.userId;
        }
    } else {
        params[@"name_full"] = self.nameTextField.text;
        params[@"email"] =  self.emailTextField.text;
        params[@"login"] =  self.emailTextField.text;
        params[@"password"] = [[self.passwordTextField.text md5] md5];
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postRegistrationWithParams:params onSuccess:^(RPUserInfo *user_info, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (user_info) {
            [self saveUser];
            [self goToUserOffice];
        } else {
            [self showMessage:error.msg];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:kErrorMessage];
    }];
}

- (void)saveUser{
    // сохраняем данные для последующего входа
    if (self.isSocial) {
        NSString *password = [[[NSString stringWithFormat:@"%@%@",kUser.userId, [kUser.userId reverseString]]md5]md5];
        [kUserDefaults setValue:password forKey:@"password"];
        [kUserDefaults setValue:kUser.userId forKey:@"login"];
        if (kUser.socialNetwork == SocialTypeVK) {
            [kUserDefaults setValue:@"vk" forKey:@"socialNetwork"];
        } else {
            [kUserDefaults setValue:@"fb" forKey:@"socialNetwork"];
        }
    } else {
        [kUserDefaults setValue:[[self.passwordTextField.text md5]md5] forKey:@"password"];
        [kUserDefaults setValue:self.emailTextField.text forKey:@"login"];
    }
    [kUserDefaults synchronize];
}

- (void)goToUserOffice{
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"Start" bundle:nil]instantiateViewControllerWithIdentifier:@"user007"];
    self.navigationController.viewControllers = @[vc, [self.navigationController.viewControllers lastObject]];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
