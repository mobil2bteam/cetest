//
//  UIView+Shake.h
//  Е-класс
//
//  Created by Ruslan on 3/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shake)

- (void)shake;

@end
