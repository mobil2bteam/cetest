//
//  RPClassList.m
//  Cetest
//
//  Created by Ruslan on 3/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPClassList.h"

@implementation RPClassList

+ (RPClassList*) sharedClassList {
    static RPClassList* classList = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        classList = [[RPClassList alloc] init];
    });
    return classList;
}

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[RPClass class] forKeyPath:@"class_list"];
    }];
}

+ (NSInteger)priceForClass:(NSString *)className {
    for (RPClass *class in [RPClassList sharedClassList].class_list) {
        if ([class.name isEqualToString:className]) {
            return class.price;
        }
    }
    return 0;
}

+ (NSString *)productKeyForClassWithID:(NSInteger)ID {
    for (RPClass *class in [RPClassList sharedClassList].class_list) {
        if (class.ID == ID) {
            return class.key_ios;
        }
    }
    return @"";
}



@end

@implementation RPClass

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"key_ios", @"active", @"name", @"price"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end


