//
//  RPStatisticTestVC+UICollectionView.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPStatisticTestVC+UICollectionView.h"
#import "RPTopResultCollectionViewCell.h"
#import "RPResultCollectionViewCell.h"
#import "RPUserInfo.h"

@implementation RPStatisticTestVC (UICollectionView)

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.resultsCollectionView) {
        return self.resultsArray.count;
    }
    return self.results.count;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.resultsCollectionView) {
        return CGSizeMake(CGRectGetWidth(self.resultsCollectionView.bounds), 50);
    }
    return CGSizeMake((CGRectGetWidth(self.topResultsCollectionView.bounds) - 5) / self.results.count, CGRectGetHeight(self.topResultsCollectionView.bounds));
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.resultsCollectionView) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 5);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    // parse date
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:self.resultsArray[indexPath.row].date_reg];

    if (collectionView == self.resultsCollectionView){
        RPResultCollectionViewCell *cell = (RPResultCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPResultCollectionViewCell class]) forIndexPath:indexPath];
        
        // convert to local date
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MMMM yyyy"];
        NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [dateFormatter setLocale:ruLocale];
        NSString *dateString = [dateFormatter stringFromDate:date];
        
        cell.ballLabel.text = [NSString stringWithFormat:@"%ld", (long)self.resultsArray[indexPath.row].sp];
        if (self.resultsArray[indexPath.row].sp == self.maxValue) {
            cell.bestResultLabel.hidden = NO;
        } else {
            cell.bestResultLabel.hidden = YES;
        }
        if (self.isTest) {
            cell.themeLabel.text = self.resultsArray[indexPath.row].theme_name;
            cell.bestResultLabel.hidden = YES;
            cell.dateLabel.text = @"";
        } else {
            cell.dateLabel.text = dateString;
            cell.themeLabel.text = @"";
        }
        return cell;
    }
    RPTopResultCollectionViewCell *cell = (RPTopResultCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPTopResultCollectionViewCell class]) forIndexPath:indexPath];
    if (self.isTest) {
        cell.themeLabel.text = self.resultsArray[indexPath.row].theme_name;
    } else {
        // convert to local date
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd"];
        NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [dateFormatter setLocale:ruLocale];
        NSString *dateString = [dateFormatter stringFromDate:date];
        cell.dayLabel.text = dateString;
        [dateFormatter setDateFormat:@"MM.yyyy"];
        
        dateString = [dateFormatter stringFromDate:date];
        cell.yearLabel.text = dateString;
    }
    return cell;
}

@end
