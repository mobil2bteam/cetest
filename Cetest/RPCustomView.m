//
//  RPCustomView.m
//  Cetest
//
//  Created by Ruslan on 3/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPCustomView.h"

@implementation RPCustomView
- (void)drawRect:(CGRect)rect {
    CAShapeLayer *mask = [[CAShapeLayer alloc] init];
    mask.frame = self.layer.bounds;
    CGFloat width = self.layer.frame.size.width;
    CGFloat height = self.layer.frame.size.height;
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, nil, 0, 0);
    CGPathAddLineToPoint(path, nil, width/2-10, 0);
    CGPathAddQuadCurveToPoint(path, nil, width/2, 30, width/2+10, 0);
    CGPathAddLineToPoint(path, nil, width/2+10, 0);
    CGPathAddLineToPoint(path, nil, width, 0);
    CGPathAddLineToPoint(path, nil, width, height);
    CGPathAddLineToPoint(path, nil, 0, height);
    CGPathCloseSubpath(path);
    mask.path = path;
    CGPathRelease(path);
    self.layer.mask = mask;
}

@end
