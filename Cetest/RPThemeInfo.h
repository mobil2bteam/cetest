//
//  RPThemeInfo.h
//  Cetest
//
//  Created by Ruslan on 3/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"
#import "RPFullTest.h"

@class Options,Info,Subject;

@interface RPThemeInfo : NSObject <EKMappingProtocol>

@property (nonatomic, strong) Options *options;

@property (nonatomic, strong) Info *info;

@property (nonatomic, strong) Subject *subject;

+(EKObjectMapping *)objectMapping;

@end


@interface Subject : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger tp;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger sp;

@property (nonatomic, copy) NSString *url_web;

@property (nonatomic, copy) NSString *result_key;

@property (nonatomic, copy) NSString *url_file;

@property (nonatomic, assign) NSInteger procent;

@property (nonatomic, copy) NSString *discription;

@property (nonatomic, copy) NSString *name;

+(EKObjectMapping *)objectMapping;

@end

