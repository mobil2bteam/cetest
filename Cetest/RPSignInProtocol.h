//
//  RPSignInProtocol.h
//  Е-класс
//
//  Created by Ruslan on 9/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RPSignInProtocol <NSObject>

- (void)registration;

- (void)authorization;

@end
