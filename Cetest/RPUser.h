//
//  RPUser.h
//  Cetest
//
//  Created by Ruslan on 3/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VKAuthorizationResult;

typedef NS_ENUM(NSInteger, SocialType) {
    SocialTypeEmail,
    SocialTypeVK,
    SocialTypeFaceBook
};

@interface RPUser : NSObject

@property (nonatomic, copy) NSString *photoUrl;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *token;

@property (nonatomic, copy) NSString *email;

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) BOOL isLogin;

@property (nonatomic, assign) SocialType socialNetwork;

+ (RPUser *)sharedUser;

+ (NSString *)imei;

+ (void)logOut;

+ (void)saveUserWithFB:(id)result;

+ (void)saveUserWithVK;

+ (void)saveUserWithVK:(VKAuthorizationResult *)result;

@end
