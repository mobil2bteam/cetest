//
//  RPRestorePasswordViewController.m
//  Е-класс
//
//  Created by Ruslan on 8/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRestorePasswordViewController.h"

@interface RPRestorePasswordViewController ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UIButton *restoreButton;

@end

@implementation RPRestorePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.restoreButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.restoreButton.titleLabel.numberOfLines = 2;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.emailTextField becomeFirstResponder];
}

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)restorePasswordButtonPressed:(id)sender {
    if (self.emailTextField.text.length) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.emailTextField resignFirstResponder];
        [self.delegate restorePasswordWithEmail:self.emailTextField.text];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
