//
//  RPShowTestController.m
//  Cetest
//
//  Created by Ruslan on 4/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRunTestController.h"
#import "Constants.h"
#import "RPUser.h"
#import "Test.h"

@implementation RPRunTestController

- (void)viewDidLoad{
    [super viewDidLoad];
    NSString *udid = [UIDevice currentDevice].identifierForVendor.UUIDString;
    if (self.isLocalLink) {
        NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        //
        NSURL* fileURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@/%@/%@/index.html?user_id=%@&password=%@&theme_id=%@&imei=%@", documentsDirectory, self.localTest.userID, self.localTest.classID, self.localTest.testID, self.localTest.themeID, self.localTest.filePath, self.localTest.userID, [kUserDefaults valueForKey:@"password"], self.localTest.themeID, udid]];
        NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
        [self.webView loadRequest:request];
        //
    } else {
        if (kUser.isLogin){
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?user_id=%ld&password=%@&theme_id=%ld&imei=%@",@"http://eclass.cetest.ru/sc/", self.link, (long)kUser.ID, [kUserDefaults valueForKey:@"password"], (long)self.themeID, udid ]]]];
        } else {
            [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?theme_id=%ld&imei=%@",@"http://eclass.cetest.ru/sc/", self.link, (long)self.themeID, udid]]]];
        }

    }
}

@end
