//
//  RPRestorePasswordViewController.h
//  Е-класс
//
//  Created by Ruslan on 8/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPRestorePasswordProtocol.h"

@interface RPRestorePasswordViewController : UIViewController

@property (weak, nonatomic) id<RPRestorePasswordProtocol> delegate;

@end
