//
//  UIImage+CropImage.h
//  Cetest
//
//  Created by Ruslan on 3/31/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (CropImage)

- (UIImage *)cropToSquare;

@end
