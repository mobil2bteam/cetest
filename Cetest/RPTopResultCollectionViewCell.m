//
//  RPTopResultCollectionViewCell.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPTopResultCollectionViewCell.h"

@implementation RPTopResultCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)prepareForReuse{
    self.dayLabel.text = @"";
    self.yearLabel.text = @"";
    self.themeLabel.text = @"";
}

@end
