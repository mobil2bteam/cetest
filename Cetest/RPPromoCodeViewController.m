//
//  RPPromoCodeViewController.m
//  Cetest
//
//  Created by Ruslan on 3/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPPromoCodeViewController.h"
#import "RPTestBoughtController.h"
#import "Header.h"

@interface RPPromoCodeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *numberButtonsArray;

@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@end

@implementation RPPromoCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.view removeGestureRecognizer:self.navigationController.interactivePopGestureRecognizer];
    [[self.clearButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    self.navigationItem.title = @"Промо код";
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    for (UIButton *button in self.numberButtonsArray) {
        button.layer.cornerRadius = CGRectGetWidth(button.bounds)/2.0;
    }
}

#pragma mark - Methods

- (void)prepareNumberButtons{
    UIButton *b = [[UIButton alloc]init];
    
    for (UIButton *button in self.numberButtonsArray) {
        button.layer.borderColor = b.tintColor.CGColor;
        [button setHighlighted:NO];
        button.layer.borderWidth = 1.0;
        button.layer.masksToBounds = YES;
        button.layer.cornerRadius = CGRectGetWidth(button.bounds)/2.0;
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [button setBackgroundColor:b.tintColor forState:UIControlStateHighlighted];
    }
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [self prepareNumberButtons];
}

#pragma mark - Actions

- (IBAction)numberButtonPressed:(UIButton *)sender {
    if (self.promoCodeLabel.text.length < 10) {
        self.promoCodeLabel.text = [self.promoCodeLabel.text stringByAppendingString:[NSString stringWithFormat:@"%ld", (long)sender.tag]];
    }
    if (self.promoCodeLabel.text.length) {
        self.clearButton.hidden = NO;
    }
}

- (IBAction)clearButtonPressed:(id)sender {
    if (self.promoCodeLabel.text.length > 0) {
        self.promoCodeLabel.text = [self.promoCodeLabel.text substringToIndex:self.promoCodeLabel.text.length-1];
    }
    if (!self.promoCodeLabel.text.length) {
        self.clearButton.hidden = YES;
    }
}

- (IBAction)nextButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postBuyTestWithID:self.currentTest.info.ID promoCode:self.promoCodeLabel.text onSuccess:^(RPError *error, RPResult *result) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        if (error) {
            [weakSelf showMessage:error.msg];
        } else {
            if ([result.result isEqualToString:@"ok"]) {
                [weakSelf performSegueWithIdentifier:@"testBoughtSegue" sender:self];
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [weakSelf showMessage:kErrorMessage];
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"testBoughtSegue"]) {
        RPTestBoughtController *newController = segue.destinationViewController;
        newController.currentTest = self.currentTest;
        newController.isBuyViaPromoCode = YES;
        newController.promoCode = self.promoCodeLabel.text;
    }
}
@end
