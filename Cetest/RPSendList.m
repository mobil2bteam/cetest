//
//  RPSendList.m
//  Cetest
//
//  Created by Ruslan on 3/24/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSendList.h"
#import "RPUserInfo.h"

@implementation RPSendList
+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[Send_List class] forKeyPath:@"send_list" forProperty:@"send_list"];
    }];
}
@end


//@implementation RPSend
//+(EKObjectMapping *)objectMapping
//{
//    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
//        [mapping mapPropertiesFromArray:@[@"user_id",
//                                          @"active",
//                                          @"name",
//                                          @"email",
//                                          @"date"]];
//        [mapping mapKeyPath:@"id" toProperty:@"ID"];
//    }];
//}
//@end
