//
//  RPSupportViewController.m
//  Е-класс
//
//  Created by Ruslan on 4/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPSupportViewController.h"
#import "UIView+Shake.h"
#import "RPServerManager.h"
#import "RPError.h"
#import "Header.h"

@interface RPSupportViewController ()

@property (weak, nonatomic) IBOutlet UIView *nameView;

@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *feedbackView;

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextView *feedbackTextView;

@end

@implementation RPSupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isFeedback) {
        self.textLabel.text = @"Отзыв о приложении";
        self.navigationItem.title = @"Отзыв";
    }
    for (UIView *view in @[self.nameView, self.feedbackView, self.emailView]) {
        view.layer.borderWidth = 1.0;
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = YES;
        view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Action 

- (IBAction)sendFeedback:(id)sender {
    if (!self.nameTextField.text.length) {
        [self.nameView shake];
        return;
    }
    if (!self.emailTextField.text.length) {
        [self.emailView shake];
        return;
    }
    if (!self.feedbackTextView.text.length) {
        [self.feedbackView shake];
        return;
    }
    NSDictionary *params;
    if (self.isFeedback){
        params = @{@"type":@"set_support_message",
                   @"user_name":self.nameTextField.text,
                   @"user_email":self.emailTextField.text,
                   @"comment":self.feedbackTextView.text};
    } else {
        params = @{@"type":@"set_app_comment",
                   @"user_name":self.nameTextField.text,
                   @"user_email":self.emailTextField.text,
                   @"ball":@(self.rating),
                   @"comment":self.feedbackTextView.text};
    }
    NSLog(@"params - %@", params);
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postSetAppCommentWithParams:params onSuccess:^(RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self showMessage:error.msg];
            if (![error.type isEqualToString:@"error"]) {
                self.nameTextField.text = @"";
                self.emailTextField.text = @"";
                self.feedbackTextView.text = @"";
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [self sendFeedback:nil];
        }];
    }];
}
@end
