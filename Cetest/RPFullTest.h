//
//  RPFullTest.h
//  Cetest
//
//  Created by Ruslan on 3/23/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class Options,Info,Message,Theme_List;

@interface RPFullTest : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<Theme_List *> *theme_list;

@property (nonatomic, strong) Options *options;

@property (nonatomic, strong) Info *info;

@property (nonatomic, strong) Message *message;

+(EKObjectMapping *)objectMapping;

@end

@interface Options : NSObject <EKMappingProtocol>

@property (nonatomic, assign) BOOL free_test;

@property (nonatomic, assign) BOOL theme_open;

@property (nonatomic, assign) BOOL free_open;

@property (nonatomic, assign) NSInteger free_day_count;

@property (nonatomic, assign) BOOL buy;

+(EKObjectMapping *)objectMapping;

@end

@interface Info : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger discipline_id;

@property (nonatomic, copy) NSString *class_name;

@property (nonatomic, copy) NSString *discipline_name;

@property (nonatomic, copy) NSString *discription;

@property (nonatomic, copy) NSString *key_ios;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, copy) NSString *key_android;

@property (nonatomic, copy) NSString *test_name;

@property (nonatomic, assign) NSInteger class_id;

@property (nonatomic, copy) NSString *discipline_icon_url;

@property (nonatomic, copy) NSString *discipline_img_url;

@property (nonatomic, assign) NSInteger free;

+(EKObjectMapping *)objectMapping;

@end


@interface Message : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *text;

+(EKObjectMapping *)objectMapping;

@end


@interface Theme_List : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *discription;

@property (nonatomic, assign) NSInteger tp;

@property (nonatomic, assign) NSInteger sp;

@property (nonatomic, assign) NSInteger ps;

@property (nonatomic, assign) NSInteger procent;

@property (nonatomic, assign) NSInteger variant;

+(EKObjectMapping *)objectMapping;

@end

