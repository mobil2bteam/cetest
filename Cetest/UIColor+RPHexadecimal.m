//
//  UIViewController+RPHexadecimal.m
//  Cetest
//
//  Created by Ruslan on 3/28/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIColor+RPHexadecimal.h"

@implementation UIColor (RPHexadecimal)

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
