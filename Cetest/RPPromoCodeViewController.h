//
//  RPPromoCodeViewController.h
//  Cetest
//
//  Created by Ruslan on 3/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPFullTest;

@interface RPPromoCodeViewController : UIViewController

@property (nonatomic, strong) RPFullTest *currentTest;

@end
