//
//  RPResult.h
//  Cetest
//
//  Created by Ruslan on 3/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface RPResult : NSObject

@property (nonatomic, copy) NSString *result;

@property (nonatomic, copy) NSString *type;

+(EKObjectMapping *)objectMapping;

@end
