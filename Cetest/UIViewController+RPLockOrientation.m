//
//  UIViewController+RPLockOrientation.m
//  Cetest
//
//  Created by Ruslan on 4/16/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIViewController+RPLockOrientation.h"
#import "Constants.h"

@implementation UIViewController (RPLockOrientation)

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    if (IS_IPAD) {
        return UIInterfaceOrientationMaskAll;
    } else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

- (BOOL)shouldAutorotate
{
    if (IS_IPAD) {
        return YES;
    } else {
        return NO;
    }
}

@end
