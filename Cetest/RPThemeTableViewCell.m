//
//  RPThemeTableViewCell.m
//  Cetest
//
//  Created by Ruslan on 3/18/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPThemeTableViewCell.h"

@implementation RPThemeTableViewCell

- (void)awakeFromNib{
    [super awakeFromNib];
    self.themeLabel.numberOfLines = 0;
    self.themeLabel.textAlignment = NSLineBreakByWordWrapping;
    self.resultLabel.textColor = [UIColor lightGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
//        self.themeLabel.textColor = [UIColor orangeColor];
    } else {
        self.themeLabel.textColor = [UIColor colorWithRed:0.553 green:0.553 blue:0.553 alpha:1.00];
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
//        self.themeLabel.textColor = [UIColor orangeColor];
    } else {
        self.themeLabel.textColor = [UIColor colorWithRed:0.553 green:0.553 blue:0.553 alpha:1.00];
    }
}

- (void)prepareForReuse{
    self.topLineView.hidden = YES;
    self.bottomLineView.hidden = YES;
    self.resultLabel.text = @"0";
    self.resultLabel.textColor = [UIColor lightGrayColor];
}

@end
