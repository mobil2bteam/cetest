//
//  RPError.h
//  Cetest
//
//  Created by Ruslan on 3/24/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface RPError : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger code;

@property (nonatomic, copy) NSString *msg;

@property (nonatomic, copy) NSString *type;

@end
