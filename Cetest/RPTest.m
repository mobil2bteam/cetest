//
//  RPFullClass.m
//  Cetest
//
//  Created by Ruslan on 3/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPTest.h"

@implementation RPTest

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"buy_id",
                                          @"class_id",
                                          @"class_name",
                                          @"discipline_color",
                                          @"discipline_font_color2",
                                          @"discipline_font_color1",
                                          @"discipline_icon_url",
                                          @"discipline_id",
                                          @"discipline_img_url",
                                          @"discipline_name",
                                          @"key_ios",
                                          @"free_date_day",
                                          @"test_name",
                                          @"image",
                                          @"price"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end
