//
//  RPFullClass.h
//  Cetest
//
//  Created by Ruslan on 3/19/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface RPTest : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger buy_id;

@property (nonatomic, assign) NSInteger class_id;

@property (nonatomic, assign) NSInteger free_date_day;

@property (nonatomic, copy) NSString *key_ios;

@property (nonatomic, copy) NSString *discipline_color;

@property (nonatomic, copy) NSString *discipline_font_color1;

@property (nonatomic, copy) NSString *discipline_font_color2;

@property (nonatomic, copy) NSString *discipline_name;

@property (nonatomic, copy) NSString *test_name;

@property (nonatomic, copy) NSString *class_name;

@property (nonatomic, copy) NSString *discipline_img_url;

@property (nonatomic, assign) NSInteger discipline_id;

@property (nonatomic, copy) NSString *discipline_icon_url;

@property (nonatomic, copy) NSString *key_android;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, assign) NSInteger ID;

+(EKObjectMapping *)objectMapping;

@end
