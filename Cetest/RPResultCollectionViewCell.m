//
//  RPResultCollectionViewCell.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPResultCollectionViewCell.h"

@implementation RPResultCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.ballLabel.textColor = [UIColor lightGrayColor];
}

@end
