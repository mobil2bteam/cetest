//
//  RPResult.m
//  Cetest
//
//  Created by Ruslan on 3/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPResult.h"

@implementation RPResult

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"result",
                                          @"type"]];
    }];
}

@end
