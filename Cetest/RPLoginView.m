//
//  RPLoginView.m
//  Cetest
//
//  Created by Ruslan on 3/22/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPLoginView.h"

@implementation RPLoginView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.layer.borderColor = [UIColor colorWithRed:0.5629 green:0.5932 blue:0.5987 alpha:1.0].CGColor;
        self.layer.borderWidth = 2.0;
        self.layer.shadowOffset = CGSizeMake(2, 3);
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        self.layer.masksToBounds = NO;
    }
    return self;
}
@end
