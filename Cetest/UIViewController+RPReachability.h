//
//  UIViewController+RPReachability.h
//  Cetest
//
//  Created by Ruslan on 3/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (RPReachability)

- (BOOL)isReachility;

@end
