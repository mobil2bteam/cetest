//
//  RPInfoViewController.m
//  Е-класс
//
//  Created by Ruslan on 4/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPInfoViewController.h"
#import "StarRatingView.h"
#import "RPSupportViewController.h"
#import "RPAboutAppViewController.h"

@interface RPInfoViewController ()

@property (weak, nonatomic) IBOutlet UIView *ratingView;

@property (strong, nonatomic) StarRatingView *starview;

@end

@implementation RPInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.starview = [[StarRatingView alloc]initWithFrame:self.ratingView.bounds andRating:5 withLabel:NO animated:NO];
    [self.ratingView addSubview:self.starview];
}

#pragma mark - Actions

- (IBAction)supportButtonPressed:(id)sender {
    
}

- (IBAction)aboutAppButtonPressed:(id)sender {
    RPAboutAppViewController *vc = [[RPAboutAppViewController alloc]initWithNibName:@"RPAboutAppViewController" bundle:nil];
    vc.isHelp = NO;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)helpButtonPressed:(id)sender {
    RPAboutAppViewController *vc = [[RPAboutAppViewController alloc]initWithNibName:@"RPAboutAppViewController" bundle:nil];
    vc.isHelp = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)rateButtonPressed:(id)sender {
    if (self.starview.rating < 80) {
        RPSupportViewController *vc = [[UIStoryboard storyboardWithName:@"Help" bundle:nil]instantiateViewControllerWithIdentifier:@"RPSupportViewController"];
        vc.isFeedback = YES;
        vc.rating = self.starview.rating / 5;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        NSString *str = @"https://itunes.apple.com/app/id1114776504";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: str] options:@{} completionHandler:nil];
    }
}

@end
