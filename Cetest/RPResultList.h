//
//  RPResultList.h
//  Cetest
//
//  Created by Ruslan on 4/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface RPResultList : NSObject<EKMappingProtocol>

@property (nonatomic, assign) NSInteger class_id;

@property (nonatomic, assign) NSInteger ps;

@property (nonatomic, assign) NSInteger tp;

@property (nonatomic, assign) NSInteger sp;

@property (nonatomic, assign) NSInteger theme_id;

@property (nonatomic, copy) NSString *class_name;

@property (nonatomic, assign) NSInteger procent;

@property (nonatomic, copy) NSString *test_name;

@property (nonatomic, copy) NSString *theme_name;

@property (nonatomic, assign) NSInteger test_id;

@end


