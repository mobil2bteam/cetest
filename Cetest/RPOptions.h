//
//  RPOptions.h
//  Е-класс
//
//  Created by Ruslan on 3/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPClass;
@class RPTest;


@interface RPOptions : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPClass *> *class_list;

@property (nonatomic, strong) NSArray<RPTest *> *discipline_list;

@property (nonatomic, strong) NSArray<RPTest *> *test_list;

@property (nonatomic, strong) NSArray<RPTest *> *buy_test_list;

@property (nonatomic, strong) NSArray<RPTest *> *free_test_list;

+(EKObjectMapping *)objectMapping;

@end

//
//@interface RPClass : NSObject <EKMappingProtocol>
//
//@property (nonatomic, assign) NSInteger ID;
//
//@property (nonatomic, assign) NSInteger active;
//
//@property (nonatomic, assign) NSInteger price;
//
//@property (nonatomic, copy) NSString *key_android;
//
//@property (nonatomic, copy) NSString *name;
//
//@property (nonatomic, copy) NSString *key_ios;
//
//+(EKObjectMapping *)objectMapping;
//
//@end
//
//
//@interface RPTest : NSObject <EKMappingProtocol>
//
//@property (nonatomic, assign) NSInteger buy_id;
//
//@property (nonatomic, assign) NSInteger class_id;
//
//@property (nonatomic, assign) NSInteger free_date_day;
//
//@property (nonatomic, copy) NSString *key_ios;
//
//@property (nonatomic, copy) NSString *discipline_color;
//
//@property (nonatomic, copy) NSString *discipline_font_color1;
//
//@property (nonatomic, copy) NSString *discipline_font_color2;
//
//@property (nonatomic, copy) NSString *discipline_name;
//
//@property (nonatomic, copy) NSString *test_name;
//
//@property (nonatomic, copy) NSString *class_name;
//
//@property (nonatomic, copy) NSString *discipline_img_url;
//
//@property (nonatomic, assign) NSInteger discipline_id;
//
//@property (nonatomic, copy) NSString *discipline_icon_url;
//
//@property (nonatomic, copy) NSString *key_android;
//
//@property (nonatomic, copy) NSString *image;
//
//@property (nonatomic, assign) NSInteger price;
//
//@property (nonatomic, assign) NSInteger ID;
//
//+(EKObjectMapping *)objectMapping;
//
//@end
