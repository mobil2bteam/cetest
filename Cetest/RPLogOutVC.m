//
//  RPLogOutVC.m
//  Е-класс
//
//  Created by Ruslan on 3/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPLogOutVC.h"

@interface RPLogOutVC ()

@end

@implementation RPLogOutVC


- (IBAction)noButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)yesButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        weakSelf.logOutCallback();
    }];
}

@end
