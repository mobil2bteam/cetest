//
//  UIViewController+RPCategory.h
//  Cetest
//
//  Created by Ruslan on 3/11/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kWarningMessage @"Заполните необходимые поля"
#define kErrorMessage @"Произошла ошибка"
#define kConnectionErrorMessage @"Проверьте, пожалуйста, соединение с интернетом"

@interface UIViewController (RPCategory)

- (void)showMessage:(NSString *)message;

- (void)showMessage:(NSString *)message
      withOkHandler:(void (^)(UIAlertAction *action))okHandler;

- (void)showMessage:(NSString *)message
      withOkHandler:(void (^)(UIAlertAction *action))okHandler
   andRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler;

- (void)showMessage:(NSString *)message
  withRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler
     andBackHandler:(void (^)(UIAlertAction *action))backHandler;

- (void)showMessage:(NSString *)message
  withRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler;

- (UIViewController*) topMostController;

@end
