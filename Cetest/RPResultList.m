//
//  RPResultList.m
//  Cetest
//
//  Created by Ruslan on 4/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPResultList.h"

@implementation RPResultList

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"class_id",
                                          @"class_name",
                                          @"procent",
                                          @"test_id",
                                          @"test_name",
                                          @"theme_id",
                                          @"theme_name",
                                          @"tp",
                                          @"ps",
                                          @"sp"]];
    }];
}

@end


