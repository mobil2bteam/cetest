//
//  RPSocialButton.m
//  Cetest
//
//  Created by Ruslan on 3/22/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPSocialButton.h"
#import "Constants.h"

@implementation RPSocialButton

//- (void)drawRect:(CGRect)rect {
//    // Drawing code
//}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSInteger topFontSize = (kIsIphone5 || kIsIphone4) ? 15 : 16;
        NSInteger bottomFontSize = (kIsIphone5 || kIsIphone4) ? 19 : 20;
        NSRange range = [self.titleLabel.text rangeOfString:@" "];
        NSString *aString = [NSString stringWithFormat:@"%@\n", [self.titleLabel.text substringToIndex:range.location]];
        [self.titleLabel setNumberOfLines:2];
        [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
        UIFont *systemFont = [UIFont systemFontOfSize:topFontSize];
        NSDictionary *systemDict = [NSDictionary dictionaryWithObject: systemFont forKey:NSFontAttributeName];
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString: aString attributes: systemDict];
        UIFont *systemFont2 = [UIFont boldSystemFontOfSize:bottomFontSize];
        NSDictionary *systemDict2 = [NSDictionary dictionaryWithObject:systemFont2 forKey:NSFontAttributeName];
        NSString *text = [self.titleLabel.text substringFromIndex:range.location];
        NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString:text attributes:systemDict2];
        [aAttrString appendAttributedString:vAttrString];
        [self.titleLabel setAttributedText:aAttrString];
        [self setAttributedTitle:aAttrString forState:UIControlStateNormal];
    }
    return self;
}

@end
