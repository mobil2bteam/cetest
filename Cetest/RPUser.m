//
//  RPUser.m
//  Cetest
//
//  Created by Ruslan on 3/5/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPUser.h"
#import "Header.h"
#import "Constants.h"

#define kUser [RPUser sharedUser]

@implementation RPUser
+ (RPUser*) sharedUser {
    static RPUser* user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[RPUser alloc] init];
    });
    return user;
}

+ (NSString *)imei{
    if ([kUserDefaults valueForKey:@"imei"]) {
        return [kUserDefaults valueForKey:@"imei"];
    } else {
        NSString *imei = [UIDevice currentDevice].identifierForVendor.UUIDString;
        [kUserDefaults setObject:imei forKey:@"imei"];
        [kUserDefaults synchronize];
        return imei;
    }
}
+ (void)logOut {
    [VKSdk forceLogout];
    [[FBSDKLoginManager new] logOut];
    APP_DELEGATE.buyTestArray = nil;
    [kUserDefaults removeObjectForKey:@"login"];
    [kUserDefaults removeObjectForKey:@"password"];
    [kUserDefaults removeObjectForKey:@"socialNetwork"];
    [kUserDefaults synchronize];
    APP_DELEGATE.user_info = nil;
    kUser.isLogin = NO;
    kUser.ID = 0;
    kUser.name = kUser.photoUrl = kUser.name = kUser.token = kUser.email = kUser.userId = nil;
}

+ (void)saveUserWithFB:(id)result{
    kUser.email = result[@"email"];
    kUser.photoUrl = [result valueForKeyPath:@"picture.data.url"];
    kUser.userId = result[@"id"];
    kUser.name = result[@"first_name"];
    kUser.socialNetwork = SocialTypeFaceBook;
}

+ (void)saveUserWithVK{
    kUser.userId = [VKSdk accessToken].userId;
    kUser.photoUrl = [VKSdk accessToken].localUser.photo_200;
    kUser.name = [VKSdk accessToken].localUser.first_name;
    kUser.email = [VKSdk accessToken].email;
    kUser.socialNetwork = SocialTypeVK;
}

+ (void)saveUserWithVK:(VKAuthorizationResult *)result{
    kUser.userId = result.token.userId;
    kUser.photoUrl = result.user.photo_200;
    kUser.name = result.user.first_name;
    kUser.email = result.token.email;
    kUser.socialNetwork = SocialTypeVK;
}

@end
