//
//  RPResultStep2ViewController.h
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ResultServerModel;

@interface RPResultStep2ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *resultsCollectionView;

@property (strong, nonatomic) NSArray <ResultServerModel *> *filteredTests;

@end
