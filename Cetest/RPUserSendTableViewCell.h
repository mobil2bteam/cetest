//
//  RPUserSendTableViewCell.h
//  Cetest
//
//  Created by Ruslan on 4/13/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPUserSendTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UISwitch *sendSwitch;
@property (weak, nonatomic) IBOutlet UIButton *deleteSendButton;
@end
