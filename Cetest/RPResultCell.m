//
//  RPResultCell.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPResultCell.h"

@implementation RPResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.ballLabel.textColor = [UIColor lightGrayColor];
}

- (void)prepareForReuse{
    self.ballLabel.text = @"";
}

@end
