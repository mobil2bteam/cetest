//
//  RPLogInVC.m
//  Е-класс
//
//  Created by Ruslan on 3/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPLogInVC.h"
#import "RPSignInVC.h"
#import "UIColor+RPHexadecimal.h"
#import "Header.h"
#import "RPServerManager.h"
#import "RPRestorePasswordViewController.h"

@interface RPLogInVC () <VKSdkDelegate, VKSdkUIDelegate, RPRestorePasswordProtocol>

@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *passwordView;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *logInButton;

@property (weak, nonatomic) IBOutlet UIButton *vkButton;

@property (weak, nonatomic) IBOutlet UIButton *fbButton;

@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@end

@implementation RPLogInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.backBarButtonItem.title = @"";

    for (UIView *view in @[self.emailView, self.passwordView]) {
        view.layer.borderColor = [UIColor grayColor].CGColor;
        view.layer.borderWidth = 1.f;
        view.layer.masksToBounds = YES;
    }
    
    for (UIButton *button in @[self.vkButton, self.fbButton]) {
        button.layer.cornerRadius = 22.f;
        button.layer.borderWidth = 2.f;
        button.layer.masksToBounds = YES;
    }
    self.vkButton.layer.borderColor = [UIColor colorWithHexString:@"4c709b"].CGColor;
    self.fbButton.layer.borderColor = [UIColor colorWithHexString:@"3c539a"].CGColor;

    self.registerButton.layer.borderWidth = 1.f;
    self.registerButton.layer.cornerRadius = 10.f;
    self.registerButton.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.logInButton.layer.cornerRadius = 5.f;
    self.logInButton.layer.masksToBounds = YES;
    self.logInButton.layer.borderWidth = 2.f;
    self.logInButton.layer.borderColor = [UIColor colorWithHexString:@"#00b962"].CGColor;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationItem.title = @"Авторизация";
    [self.navigationItem setHidesBackButton:YES];
    
    // register VKSdk delegate
    [[VKSdk initializeWithAppId:kVKIdentifier] registerDelegate:self];
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:kVKIdentifier];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationItem.title = @"";
}
#pragma mark - Actions

- (IBAction)vkButtonPressed:(id)sender {
    kUser.socialNetwork = SocialTypeVK;
    [VKSdk authorize:@[VK_PER_WALL, VK_PER_EMAIL]];
}

- (IBAction)fbButtonPressed:(id)sender {
    kUser.socialNetwork = SocialTypeFaceBook;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             [self showMessage:kErrorMessage];
         } else if (!result.isCancelled) {
             [self fetchUserInfo];
         }
     }];
}

- (IBAction)logInButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (self.emailTextField.text.length == 0) {
        [self.emailView shake];
        return;
    }
    if (self.passwordTextField.text.length == 0) {
        [self.passwordView shake];
        return;
    }
    [RPUser logOut];
    kUser.socialNetwork = SocialTypeEmail;
    [self authorizeUser];
}

- (IBAction)registerButtonPressed:(id)sender {
    [self goToRegistration:NO];
}

#pragma mark - VK

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{
    if (kUser.isLogin) {
        return;
    }
    if (result.token.accessToken) {
        [RPUser saveUserWithVK:result];
        [self checkIfUserWasLoggined];
    }
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.navigationController.topViewController presentViewController:controller animated:NO completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
//    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
//    [vc presentIn:self];
}

- (void)vkSdkUserAuthorizationFailed{
    [self showMessage:kErrorMessage];
}

#pragma mark - Facebook

- (void)fetchUserInfo{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, picture.type(large), email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 [RPUser saveUserWithFB:result];
                 [self checkIfUserWasLoggined];
             }
             else
             {
                 [self showMessage:kErrorMessage];
             }
         }];
    }
}

#pragma mark - Methods

- (void)checkIfUserWasLoggined{
    NSString *password = [[[NSString stringWithFormat:@"%@%@",kUser.userId, [kUser.userId reverseString]]md5]md5];
    NSString *login = kUser.userId;
    NSDictionary *params = @{@"type":@"login",
                             @"password":password,
                             @"login":login
                             };
    __weak typeof(self) weakSelf = self;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setLabelText:@"Подождите..."];
    [[RPServerManager sharedManager]postLoginWithParams:params onSuccess:^(RPUserInfo *user_info, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (user_info) {
            [self saveUser];
            [self goToUserOffice];
        } else {
            [self goToRegistration:YES];
//            [weakSelf.enterButton setTitle:@"Продолжить" forState:UIControlStateNormal];
        }
        
//        weakSelf.profileView.hidden = weakSelf.socialLoginView.hidden = NO;
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf checkIfUserWasLoggined];
        } andBackHandler:^(UIAlertAction *action) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
}

- (void)goToRegistration:(BOOL)social{
    RPSignInVC *signInVC = [[RPSignInVC alloc]initWithNibName:@"RPSignInVC" bundle:nil];
    signInVC.isSocial = social;
    [self.navigationController pushViewController:signInVC animated:YES];
}

- (void)goToUserOffice{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)saveUser{
    // сохраняем данные для последующего входа
    if (kUser.socialNetwork != SocialTypeEmail) {
        NSString *password = [[[NSString stringWithFormat:@"%@%@",kUser.userId, [kUser.userId reverseString]]md5]md5];
        [kUserDefaults setValue:password forKey:@"password"];
        [kUserDefaults setValue:kUser.userId forKey:@"login"];
        if (kUser.socialNetwork == SocialTypeVK) {
            [kUserDefaults setValue:@"vk" forKey:@"socialNetwork"];
        } else {
            [kUserDefaults setValue:@"fb" forKey:@"socialNetwork"];
        }
    } else {
        [kUserDefaults setValue:[[self.passwordTextField.text md5]md5] forKey:@"password"];
        [kUserDefaults setValue:self.emailTextField.text forKey:@"login"];
    }

    [kUserDefaults synchronize];
}

- (IBAction)forgotPasswordButtonPressed:(id)sender {
    RPRestorePasswordViewController *restorePasswordVC = [[RPRestorePasswordViewController alloc]initWithNibName:@"RPRestorePasswordViewController" bundle:nil];
    restorePasswordVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    restorePasswordVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    restorePasswordVC.delegate = self;
    [self presentViewController:restorePasswordVC animated:YES completion:nil];
}

- (void)authorizeUser{
    NSDictionary *params = @{@"type":@"login",
                             @"password":[[self.passwordTextField.text md5]md5],
                             @"login":self.emailTextField.text
                             };
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postLoginWithParams:params onSuccess:^(RPUserInfo *user_info, RPError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (user_info) {
            kUser.socialNetwork = SocialTypeEmail;
            [self saveUser];
            [self goToUserOffice];
        } else {
            [self showMessage:error.msg];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [self showMessage:kErrorMessage];
    }];
}

#pragma mark - RPRestorePasswordDelegate

- (void)restorePasswordWithEmail:(NSString *)email{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postRestorePasswordWithEmail:email onSuccess:^(RPError *message) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:message.msg];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:kErrorMessage];
    }];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
