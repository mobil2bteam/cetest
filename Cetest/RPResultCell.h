//
//  RPResultCell.h
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPResultCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@property (weak, nonatomic) IBOutlet UILabel *ballLabel;

@end
