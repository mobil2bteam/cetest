//
//  RPClassCollectionViewCell.h
//  Е-класс
//
//  Created by Ruslan on 3/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPClassCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *classLabel;

@end
