//
//  RPError.m
//  Cetest
//
//  Created by Ruslan on 3/24/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPError.h"
#import <UIKit/UIKit.h>

@implementation RPError

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
//        [mapping mapKeyPath:@"message.code" toProperty:@"code"];
//        [mapping mapKeyPath:@"message.type" toProperty:@"type"];
//        [mapping mapKeyPath:@"message.msg" toProperty:@"msg"];
        [mapping mapPropertiesFromArray:@[@"msg",
                                          @"type",
                                          @"code"]];

    }];
}

@end
