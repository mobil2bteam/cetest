//
//  RPResultHeaderCollectionViewCell.h
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPResultHeaderCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@property (assign, nonatomic) NSInteger section;

@property (nonatomic, copy) void (^callback)(NSInteger section);

@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;

@end
