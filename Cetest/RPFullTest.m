//
//  RPFullTest.m
//  Cetest
//
//  Created by Ruslan on 3/23/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPFullTest.h"

@implementation RPFullTest

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[Info class] forKeyPath:@"test.info" forProperty:@"info"];
        [mapping hasOne:[Message class] forKeyPath:@"test.message" forProperty:@"message"];
        [mapping hasOne:[Options class] forKeyPath:@"test.options" forProperty:@"options"];
        [mapping hasMany:[Theme_List class] forKeyPath:@"test.theme_list" forProperty:@"theme_list"];
    }];
}
@end

@implementation Options
+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"free_test",
                                          @"theme_open",
                                          @"free_open",
                                          @"free_day_count",
                                          @"buy"]];
    }];
}
@end

@implementation Info
+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"class_id",
                                          @"class_name",
                                          @"test_name",
                                          @"discipline_id",
                                          @"discipline_img_url",
                                          @"discipline_name",
                                          @"discipline_icon_url",
                                          @"key_ios",
                                          @"price",
                                          @"discription",
                                          @"free"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}
@end

@implementation Message
+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"title",
                                          @"text"]];
    }];
}
@end

@implementation Theme_List
+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name",
                                          @"tp",
                                          @"procent",
                                          @"variant",
                                          @"sp",
                                          @"ps",
                                          @"discription"]];
        [mapping mapKeyPath:@"id" toProperty:@"ID"];
    }];
}

@end


