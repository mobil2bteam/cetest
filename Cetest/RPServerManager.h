//
//  RPServerManager.h
//  Cetest
//
//  Created by Ruslan on 3/11/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPClassList.h"
#import "RPTest.h"
#import "RPUser.h"
#import "RPFullTest.h"
#import "RPSendList.h"
#import "RPError.h"
#import "RPUserInfo.h"
#import "RPResult.h"
#import "RPThemeInfo.h"
#import "RPResultList.h"

@interface RPServerManager : NSObject

+ (RPServerManager*) sharedManager;

- (void)getClassListOnSuccess:(void(^)(RPClassList *class_list, RPError *error)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) postRegistrationWithParams:(NSDictionary*)params
                          onSuccess:(void(^)(RPUserInfo *user_info, RPError *error)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postLoginWithParams:(NSDictionary*)params
                  onSuccess:(void(^)(RPUserInfo *user_info, RPError *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postEditUserWithName:(NSString*) name
                            email:(NSString*) email
                        onSuccess:(void(^)(RPUserInfo *user_info, RPError *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postGetUserTestsOnSuccess:(void(^)(NSArray <NSArray <RPTest *> *> *buyTestArray, NSArray <RPTest *> *freeTestArray, NSString *errorMessage)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postGetAllTestsOnSuccess:(void(^)(NSArray <NSArray <RPTest *> *> *allTestArray, RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postGetFullTestWithTestID:(NSInteger) testId
                        onSuccess:(void(^)(RPFullTest *test, RPError *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postGetSendListOnSuccess:(void(^)(RPSendList *send_list, RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postAddToSendListWithName:(NSString*) name
                            email:(NSString*) email
                        onSuccess:(void(^)(RPSendList *send_list, RPError *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postRemoveFromSendListWithID:(NSInteger) ID
                        onSuccess:(void(^)(RPSendList *send_list, RPError *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postEditSendWithName:(NSString*) name
                            email:(NSString*) email
                            ID:(NSInteger) ID
                          active:(NSInteger) active
                        onSuccess:(void(^)(RPSendList *send_list, RPError *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postBuyTestWithID:(NSInteger) testID
                      promoCode:(NSString *)promoCode
                onSuccess:(void(^)(RPError *error, RPResult *result)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postBuyClassWithID:(NSInteger) classID
                onSuccess:(void(^)(RPError *error, RPResult *result)) success
                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postGetThemeWithID:(NSInteger) themeID
                 onSuccess:(void(^)(RPError *error, RPThemeInfo *themeInfo)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postGetResultListOnSuccess:(void(^)(RPError *error, NSArray < NSArray < NSArray <RPResultList *> *> *> *results)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postChangePasswordWithOldPassword:(NSString *)oldPassword
                              newPassword:(NSString *)newPassword
                                onSuccess:(void(^)(RPError *error, RPUserInfo *user_info)) success
                                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postRestorePasswordWithEmail:(NSString *)email
                           onSuccess:(void(^)(RPError *message)) success
                                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postShowPromoCodeOnSuccess:(void(^)(RPError *error, BOOL show)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postGetAllDataOnSuccess:(void(^)(RPError *error)) success
                      onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)postSetAppCommentWithParams:(NSDictionary *) params
                          onSuccess:(void(^)(RPError *error)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

@end
