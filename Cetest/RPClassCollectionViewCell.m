//
//  RPClassCollectionViewCell.m
//  Е-класс
//
//  Created by Ruslan on 3/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPClassCollectionViewCell.h"

@implementation RPClassCollectionViewCell

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        self.contentView.backgroundColor = [UIColor blueColor];
    } else {
        self.contentView.backgroundColor = [UIColor grayColor];
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    if (highlighted) {
        self.contentView.backgroundColor = [UIColor blueColor];
    } else {
        self.contentView.backgroundColor = [UIColor grayColor];
    }
}

@end
