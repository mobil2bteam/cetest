//
//  RPResultViewController.m
//  Е-класс
//
//  Created by Ruslan on 4/22/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPResultViewController.h"
#import "RPUserInfo.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "RPResultCell.h"
#import "RPResultHeaderCollectionViewCell.h"
#import "RPResultStep2ViewController.h"
#import "RPUser.h"
#import "Constants.h"

@interface RPResultViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resultsCollectionViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *registrationView;

@property (weak, nonatomic) IBOutlet UIView *noDataView;

@end

@implementation RPResultViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    if (![APP_DELEGATE.navControllers containsObject:self.navigationController]) {
        [APP_DELEGATE.navControllers addObject:self.navigationController];
    }
    
    // register cells
    [self.resultsCollectionView registerNib:[UINib nibWithNibName:@"RPResultHeaderCollectionViewCell" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPResultHeaderCollectionViewCell class])];
    
    [self.resultsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPResultCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPResultCell class])];
    
    // add observer to automatic update resultsCollectionView's height
    [self addObserver:self forKeyPath:@"resultsCollectionView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
    
    self.filteredResults = [APP_DELEGATE.user_info groupedResultsByClasses];
    [self.resultsCollectionView reloadData];
    [self setUpView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.filteredResults = [APP_DELEGATE.user_info groupedResultsByClasses];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.resultsCollectionView reloadData];
    });
    [self setUpView];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [self.view setNeedsDisplay];
    [self.resultsCollectionView reloadData];
}

- (void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"resultsCollectionView.contentSize"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:[UIApplication sharedApplication]];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"resultsCollectionView.contentSize"]) {
        CGFloat newHeight = self.resultsCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.resultsCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

#pragma mark - Methods

- (void)showTestWithIndexPath:(NSIndexPath *)indexPath{
    RPResultStep2ViewController *vc = [[UIStoryboard storyboardWithName:@"Result" bundle:nil]instantiateViewControllerWithIdentifier:@"RPResultStep2ViewController"];
    vc.filteredTests = self.filteredResults[indexPath.section][indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setUpView{
    self.registrationView.hidden = kUser.isLogin;
    self.noDataView.hidden = self.filteredResults.count;
}

- (IBAction)registrationButtonPressed:(id)sender {
    [self.navigationController.tabBarController setSelectedIndex:2];
}

@end
