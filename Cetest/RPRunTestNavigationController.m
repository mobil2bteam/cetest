//
//  RPRunTestNavigationController.m
//  Cetest
//
//  Created by Ruslan on 4/16/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPRunTestNavigationController.h"
#import "Constants.h"

@implementation RPRunTestNavigationController

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
        return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

@end
