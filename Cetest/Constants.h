//
//  Constants.h
//  Cetest
//
//  Created by Ruslan on 3/6/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define APP_DELEGATE ((AppDelegate *)[[UIApplication sharedApplication]delegate])

#define kVKIdentifier @"5389909"
#define kAddressSite @"http://eclass.cetest.ru/sc/"

#define kSuccessGreenColor [UIColor colorWithRed:0.329 green:0.839 blue:0.580 alpha:1.00]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kIsIphone4 kScreenHeight == 480
#define kIsIphone5 kScreenHeight == 568
#define kIsLandscapeOrientation (UIInterfaceOrientationLandscapeLeft == ([[UIApplication sharedApplication] statusBarOrientation]) || UIInterfaceOrientationLandscapeRight == ([[UIApplication sharedApplication] statusBarOrientation]))
#define kAnimationDuration 0.25
#define kUser [RPUser sharedUser]
#define kAnimateDuration 0.25
#define kViewTransitionAnimationDuration 0.5f
#define kUserDefaults [NSUserDefaults standardUserDefaults]

//ключи для хранения информации
#define kWasRegisteredKey @"wasRegistered"
#define kAllClassedKey @"Все классы"
#define kSelectedClassKey @"selectedClass"
#define kHideFreeTestInfoKey @"hideFreeTestInfo"
#endif /* Constants_h */
//852781114850836
//vk5331790
