//
//  RPServerManager.m
//  Cetest
//
//  Created by Ruslan on 3/11/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
#define kUser [RPUser sharedUser]

#import "Constants.h"
#import "RPServerManager.h"
#import "AFNetworking.h"
#import "NSString+Category.h"
#import "AppDelegate.h"
#import "RPOptions.h"
#import "RPUserInfo.h"

@interface RPServerManager ()
@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;
@end

@implementation RPServerManager

+ (RPServerManager*) sharedManager {
    static RPServerManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[RPServerManager alloc] init];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", kAddressSite]];
        manager.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        manager.requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
        manager.requestOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    });
    return manager;
}

- (void)postRegistrationWithParams:(NSDictionary*)params
                         onSuccess:(void(^)(RPUserInfo *user_info, RPError *error)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPUserInfo *user_info = nil;
    __block RPError *error = nil;
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (data[@"user_info"]) {
                                      user_info = [EKMapper objectFromExternalRepresentation:data[@"user_info"]
                                                                                 withMapping:[RPUserInfo objectMapping]];
                                      APP_DELEGATE.user_info = user_info;
                                      kUser.email = user_info.email;
                                      kUser.name = user_info.name_full;
                                      kUser.ID = user_info.ID;
                                      kUser.isLogin = YES;
                                  }
                                  if (success) {
                                      success(user_info, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postLoginWithParams:(NSDictionary*)params
                  onSuccess:(void(^)(RPUserInfo *user_info, RPError *error)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPUserInfo *user_info = nil;
    __block RPError *error = nil;
    NSLog(@"params - %@", params);
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"login - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (data[@"user_info"]) {
                                      user_info = [EKMapper objectFromExternalRepresentation:data[@"user_info"]
                                                                                 withMapping:[RPUserInfo objectMapping]];
                                      APP_DELEGATE.user_info = user_info;
                                      kUser.email = user_info.email;
                                      kUser.name = user_info.name_full;
                                      kUser.ID = user_info.ID;
                                      kUser.isLogin = YES;
                                  }
                                  if (success) {
                                      success(user_info, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)getClassListOnSuccess:(void(^)(RPClassList *class_list, RPError *error)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPClassList *class_list = nil;
    __block RPError *error = nil;
    NSDictionary *params = @{@"type":@"get_class_list"};
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"classes - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (data[@"class_list"]) {
                                      class_list = [EKMapper objectFromExternalRepresentation:data
                                                                                  withMapping:[RPClassList objectMapping]];
                                      [RPClassList sharedClassList].class_list = class_list.class_list;
                                  }
                                  
                                  if (success) {
                                      success(class_list, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postGetUserTestsOnSuccess:(void(^)(NSArray <NSArray <RPTest *> *> *buyTestArray, NSArray <RPTest *> *freeTestArray, NSString *errorMessage)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block NSString *errorMessage = nil;
    __block NSMutableArray <NSMutableArray <RPTest*> *> *buyTestArray;
    __block NSMutableArray <RPTest *> *freeTestArray;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"get_user_test";
    params[@"imei"] = [RPUser imei];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    NSLog(@"params - %@", params);
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data -%@", data);
                                  if (data[@"message"][@"msg"]!=nil) {
                                      errorMessage = data[@"message"][@"msg"];
                                  } else {
                                      
                                      if (data[@"user_test_list"][@"buy_test_list"]) {
                                          buyTestArray = [NSMutableArray new];
                                          for (NSDictionary *dictioney in data[@"user_test_list"][@"buy_test_list"]) {
                                              RPTest *currentTest = [EKMapper objectFromExternalRepresentation:dictioney                                                                                                    withMapping:[RPTest objectMapping]];
                                              BOOL isAdded = NO;
                                              for (NSInteger index = 0; index < buyTestArray.count; index++) {
                                                  NSMutableArray <RPTest *> *array  = buyTestArray[index];
                                                  if ([array[0].class_name isEqualToString:currentTest.class_name]  ) {
                                                      [array addObject:currentTest];
                                                      isAdded = YES;
                                                  }
                                              }
                                              if (!isAdded) {
                                                  NSMutableArray <RPTest *> *newArray  = [NSMutableArray array];
                                                  [newArray addObject:currentTest];
                                                  [buyTestArray addObject:newArray];
                                              }
                                          }

                                      }
                                      
                                      if (data[@"user_test_list"][@"free_test_list"]) {
                                          freeTestArray = [NSMutableArray new];
                                          for (NSDictionary *dictionary in data[@"user_test_list"][@"free_test_list"]) {
                                              RPTest *currentClass = [EKMapper objectFromExternalRepresentation:dictionary
                                                                                                    withMapping:[RPTest objectMapping]];
                                              [freeTestArray addObject:currentClass];
                                          }
                                      }
                                      
                                  }
                                  if (success) {
                                      success(buyTestArray, freeTestArray, errorMessage);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postGetAllTestsOnSuccess:(void(^)(NSArray <NSArray <RPTest *> *> *allTestArray, RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error = nil;
    __block NSMutableArray <NSMutableArray <RPTest*> *> *allTestArray;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"get_all_test";
    params[@"imei"] = [RPUser imei];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"all tests - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      if (data[@"test_list"]) {
                                          allTestArray = [self getTestsFromDictionary:data[@"test_list"]];
                                      }
                                  }
                                  
                                  if (success) {
                                      success(allTestArray, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}


- (void)postGetAllDataOnSuccess:(void(^)(RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error = nil;
    __block RPOptions *options = nil;

    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"get_all_data";
    params[@"imei"] = [RPUser imei];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"all tests - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      options = [EKMapper objectFromExternalRepresentation:data
                                                                             withMapping:[RPOptions objectMapping]];
                                      APP_DELEGATE.allTestArray = [self getTestsFromList:options.test_list];
                                      APP_DELEGATE.freeTestArray = options.free_test_list;
                                      APP_DELEGATE.buyTestArray = [self getTestsFromList:options.buy_test_list];
                                      APP_DELEGATE.class_list = options.class_list;
                                      [RPClassList sharedClassList].class_list = options.class_list;
                                  }
                                  
                                  if (success) {
                                      success(error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,error.code);
                                  }
                              }];
}

- (NSMutableArray <NSMutableArray <RPTest*> *> *)getTestsFromList:(NSArray<RPTest *> *) list{
    NSMutableArray <NSMutableArray <RPTest*> *> *testArray = [NSMutableArray new];
    for (RPTest *test in list) {
        BOOL isAdded = NO;
        for (NSInteger index = 0; index < testArray.count; index++) {
            NSMutableArray <RPTest *> *array  = testArray[index];
            if ([array[0].class_name isEqualToString:test.class_name]  ) {
                [array addObject:test];
                isAdded = YES;
            }
        }
        if (!isAdded) {
            NSMutableArray <RPTest *> *newArray  = [NSMutableArray array];
            [newArray addObject:test];
            [testArray addObject:newArray];
        }
    }
    return testArray;
}

- (void)postGetFullTestWithTestID:(NSInteger) testId
                        onSuccess:(void(^)(RPFullTest *test, RPError *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error = nil;
    __block RPFullTest *test = nil;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"get_test";
    params[@"imei"] = [RPUser imei];
    params[@"id"] = @(testId);
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"full test - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      test = [EKMapper objectFromExternalRepresentation:data
                                                                            withMapping:[RPFullTest objectMapping]];
                                  }
                                  if (success) {
                                      success(test, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postGetSendListOnSuccess:(void(^)(RPSendList *send_list, RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    __block RPSendList *send_list;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"get_send_list";
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    NSLog(@"params - %@", params);
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      send_list = [EKMapper objectFromExternalRepresentation:data                                                                                 withMapping:[RPSendList objectMapping]];
                                      
                                  }
                                  if (success) {
                                      success(send_list, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postAddToSendListWithName:(NSString*) name
                            email:(NSString*) email
                        onSuccess:(void(^)(RPSendList *send_list, RPError *error)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    __block RPSendList *send_list;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"add_send_list";
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    params[@"name"] = name;
    params[@"email"] = email;
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      send_list = [EKMapper objectFromExternalRepresentation:data
                                                                                 withMapping:[RPSendList objectMapping]];
                                  }
                                  if (success) {
                                      success(send_list, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}


- (void)postRemoveFromSendListWithID:(NSInteger)ID onSuccess:(void (^)(RPSendList *, RPError *))success onFailure:(void (^)(NSError *, NSInteger))failure{
    __block RPError *error;
    __block RPSendList *send_list;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"delete_send_list";
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    params[@"id"] = @(ID);
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data send = %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      send_list = [EKMapper objectFromExternalRepresentation:data
                                                                                 withMapping:[RPSendList objectMapping]];
                                  }
                                  if (success) {
                                      success(send_list, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postEditSendWithName:(NSString *)name email:(NSString *)email ID:(NSInteger)ID active:(NSInteger)active onSuccess:(void (^)(RPSendList *, RPError *))success onFailure:(void (^)(NSError *, NSInteger))failure{
    __block RPError *error;
    __block RPSendList *send_list;
    NSMutableDictionary *params = [NSMutableDictionary new];
    params[@"type"] =  @"edit_send_list";
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    params[@"id"] = @(ID);
    params[@"name"] = name;
    params[@"email"] = email;
    params[@"active"] = @(active);
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      send_list = [EKMapper objectFromExternalRepresentation:data
                                                                                 withMapping:[RPSendList objectMapping]];
                                  }
                                  if (success) {
                                      success(send_list, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postBuyTestWithID:(NSInteger) testID
                      promoCode:(NSString *)promoCode
                onSuccess:(void(^)(RPError *error, RPResult *result)) success
                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    __block RPResult *result;
    NSMutableDictionary *params = [@{@"type" : @"set_buy_test"}mutableCopy];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    if (promoCode) {
        params[@"promo_code"] = [[NSString stringWithFormat:@"code_%@", promoCode]md5];
    }
    NSString *stringID = [NSString stringWithFormat:@"test_%ld",(long)testID];
    NSString *key = [stringID md5];
    params[@"key"] = key;
    params[@"imei"] = [RPUser imei];
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      result = [EKMapper objectFromExternalRepresentation:data withMapping:[RPResult objectMapping]];
                                  }
                                  if (success) {
                                      success(error, result);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postBuyClassWithID:(NSInteger) classID
                onSuccess:(void(^)(RPError *error, RPResult *result)) success
                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    __block RPResult *result;
    NSMutableDictionary *params = [@{@"type" : @"set_buy_class"}mutableCopy];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    NSString *stringID = [NSString stringWithFormat:@"class_%ld",(long)classID];
    NSString *key = [stringID md5];
    params[@"key"] = key;
    params[@"imei"] = [RPUser imei];
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      result = [EKMapper objectFromExternalRepresentation:data withMapping:[RPResult objectMapping]];
                                  }
                                  if (success) {
                                      success(error, result);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postGetThemeWithID:(NSInteger) themeID
                 onSuccess:(void(^)(RPError *error, RPThemeInfo *themeInfo)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    __block RPThemeInfo *themeInfo;
    NSMutableDictionary *params = [@{@"type" : @"get_theme"}mutableCopy];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    params[@"imei"] = [RPUser imei];
    params[@"id"] = @(themeID);
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"theme - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      themeInfo = [EKMapper objectFromExternalRepresentation:data withMapping:[RPThemeInfo objectMapping]];
                                      
                                  }
                                  
                                  if (success) {
                                      success(error, themeInfo);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];

}

- (void)postShowPromoCodeOnSuccess:(void(^)(RPError *error, BOOL show)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    [self.requestOperationManager GET:@"settings.html" parameters:nil
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {                                  BOOL show = NO;
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      show = [data[@"p"]boolValue];
                                  }
                                  if (success) {
                                      success(error, show);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    
}

#pragma mark - Methods

- (NSMutableArray <NSMutableArray <RPTest*> *> *)getTestsFromDictionary :(NSDictionary *)dictionary{
    NSMutableArray <NSMutableArray <RPTest*> *> *testArray = [NSMutableArray new];
    // if we need all classes
        for (NSDictionary *dictioney in dictionary) {
            RPTest *currentTest = [EKMapper objectFromExternalRepresentation:dictioney                                                                                                    withMapping:[RPTest objectMapping]];
            BOOL isAdded = NO;
            for (NSInteger index = 0; index < testArray.count; index++) {
                NSMutableArray <RPTest *> *array  = testArray[index];
                if ([array[0].class_name isEqualToString:currentTest.class_name]  ) {
                    [array addObject:currentTest];
                    isAdded = YES;
                }
            }
            if (!isAdded) {
                NSMutableArray <RPTest *> *newArray  = [NSMutableArray array];
                [newArray addObject:currentTest];
                [testArray addObject:newArray];
            }
        }
    return testArray;
}

- (void)postGetResultListOnSuccess:(void(^)(RPError *error,NSArray < NSArray < NSArray <RPResultList *> *> *> *results)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    __block NSMutableArray < NSMutableArray < NSMutableArray <RPResultList *> *> *> *results;
    NSMutableDictionary *params = [@{@"type" : @"get_my_result"}mutableCopy];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                      if (data[@"result_list"]) {
                                          results = [NSMutableArray new];
                                          for (NSDictionary *dictionary in data[@"result_list"]) {
                                              RPResultList *result = [EKMapper objectFromExternalRepresentation:dictionary withMapping:[RPResultList objectMapping]];
                                              if (!results.count) {
                                                  NSMutableArray <NSMutableArray <RPResultList *> *> *tests = [NSMutableArray new];
                                                  NSMutableArray <RPResultList *> *themes = [NSMutableArray new];
                                                  [themes addObject:result];
                                                  [tests addObject:themes];
                                                  [results addObject:tests];
                                              } else {
                                                  BOOL isAddedToClass = NO;
                                                  for (NSInteger i = 0; i < results.count; i++) {
                                                      NSMutableArray <NSMutableArray <RPResultList *> *> *test = results[i];
                                                      RPResultList *classResult = test[0][0];
                                                      // если класс уже есть
                                                      if (classResult.class_id == result.class_id) {
                                                          
                                                          BOOL isAddedToTest = NO;
                                                          for (NSInteger i = 0; i < test.count; i++) {
                                                               NSMutableArray <RPResultList *> *theme = test[i];
                                                              RPResultList *testResult = theme[0];
                                                              // если тест уже есть
                                                              if (testResult.test_id == result.test_id) {
                                                                  [theme addObject:result];
                                                                  isAddedToTest = YES;
                                                                  isAddedToClass = YES;
                                                              }
                                                          }
                                                          if (!isAddedToTest){
                                                              NSMutableArray <RPResultList *> *themeArray = [NSMutableArray new];
                                                              [themeArray addObject:result];
                                                              [test addObject:themeArray];
                                                              isAddedToClass = YES;
                                                          }
                                                      }
                                                  }
                                                  // если класса еще нет, то добавить
                                                  if (!isAddedToClass){
                                                      NSMutableArray <NSMutableArray <RPResultList *> *> *tests = [NSMutableArray new];
                                                      NSMutableArray <RPResultList *> *themes = [NSMutableArray new];
                                                      [themes addObject:result];
                                                      [tests addObject:themes];
                                                      [results addObject:tests];
                                                  }
                                              }
                                          }
                                      }

                                  }
                                  
                                  if (success) {
                                      success(error, results);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
    

}

- (void)postEditUserWithName:(NSString*) name
                            email:(NSString*) email
                        onSuccess:(void(^)(RPUserInfo *user_info, RPError *error)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPUserInfo *user_info = nil;
    __block RPError *error = nil;
    NSMutableDictionary *params = [@{@"type" : @"update"}mutableCopy];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
        params[@"password"] = [kUserDefaults valueForKey:@"password"];
    }
    params[@"name_full"] = name;
    params[@"email"] = email;
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (data[@"user_info"]) {
                                      user_info = [EKMapper objectFromExternalRepresentation:data[@"user_info"]
                                                                                 withMapping:[RPUserInfo objectMapping]];
                                      kUser.email = user_info.email;
                                      kUser.name = user_info.name_full;
                                  }
                                  if (success) {
                                      success(user_info, error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postChangePasswordWithOldPassword:(NSString *)oldPassword
                              newPassword:(NSString *)newPassword
                                onSuccess:(void(^)(RPError *error, RPUserInfo *user_info)) success
                                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    __block RPUserInfo *user_info = nil;
    NSMutableDictionary *params = [@{@"type" : @"change_password"}mutableCopy];
    if ([kUserDefaults valueForKey:@"login"]) {
        params[@"login"] = [kUserDefaults valueForKey:@"login"];
    }
    params[@"password"] = oldPassword;
    params[@"new_password"] = newPassword;
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  } else {
                                    user_info = [EKMapper objectFromExternalRepresentation:data[@"user_info"]
                                                                               withMapping:[RPUserInfo objectMapping]];
                                  }
                                  
                                  if (success) {
                                      success(error, user_info);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postRestorePasswordWithEmail:(NSString *)email
                           onSuccess:(void(^)(RPError *message)) success
                           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    __block RPError *message = nil;
    NSMutableDictionary *params = [@{@"type" : @"send_my_new_password",
                                     @"login" : email} mutableCopy];
    [self.requestOperationManager POST:@"user.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  message = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                         withMapping:[RPError objectMapping]];
                                  
                                  if (success) {
                                      success(message);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

- (void)postSetAppCommentWithParams:(NSDictionary *) params
                          onSuccess:(void(^)(RPError *error)) success
                          onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    __block RPError *error;
    
    [self.requestOperationManager POST:@"cetest.php?" parameters:params
                              progress:^(NSProgress * _Nonnull uploadProgress) {
                              } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable data) {
                                  NSLog(@"data - %@", data);
                                  if (data[@"message"]) {
                                      error = [EKMapper objectFromExternalRepresentation:data[@"message"]
                                                                             withMapping:[RPError objectMapping]];
                                  }
                                  if (success) {
                                      success(error);
                                  }
                                  
                              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                  if (failure)
                                  {
                                      failure(error,404);
                                  }
                              }];
}

@end
