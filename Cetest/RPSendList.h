//
//  RPSendList.h
//  Cetest
//
//  Created by Ruslan on 3/24/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPSend;
@class Send_List;

@interface RPSendList : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray <Send_List *> *send_list;

@end

//@interface RPSend : NSObject <EKMappingProtocol>
//
//@property (nonatomic, copy) NSString *email;
//
//@property (nonatomic, assign) NSInteger ID;
//
//@property (nonatomic, assign) NSInteger active;
//
//@property (nonatomic, assign) NSInteger user_id;
//
//@property (nonatomic, copy) NSString *name;
//
//@property (nonatomic, copy) NSString *date;
//
//@end
