//
//  RPLogOutVC.h
//  Е-класс
//
//  Created by Ruslan on 3/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPLogOutVC : UIViewController

@property (nonatomic, copy) void (^logOutCallback)(void);

@end
