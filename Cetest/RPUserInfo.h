//
//  RPUserInfo.h
//  Cetest
//
//  Created by Ruslan on 3/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class Send_List;
@class ResultServerModel;

@interface RPUserInfo : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger class_id;

@property (nonatomic, copy) NSString *vk_id;

@property (nonatomic, copy) NSString *name_full;

@property (nonatomic, copy) NSString *password;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, copy) NSString *fb_id;

@property (nonatomic, strong) NSArray<Send_List *> *send_list;

@property (nonatomic, strong) NSArray<ResultServerModel *> *last_result_list;

@property (nonatomic, strong) NSArray<ResultServerModel *> *result_list;

@property (nonatomic, copy) NSString *email;

- (NSArray *)groupedResultsByClasses;

- (NSArray *)resultsByClass:(NSString *)className andTest:(NSString *)testName;

- (NSArray *)resultsByClass:(NSString *)className andTest:(NSString *)testName andTheme:(NSString *)themeName;

@end


@interface Send_List : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *email;

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger active;

@property (nonatomic, assign) NSInteger user_id;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *date;

@end


@interface ResultServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger procent;

@property (nonatomic, assign) NSInteger tp;

@property (nonatomic, assign) NSInteger sp;

@property (nonatomic, assign) NSInteger ps;

@property (nonatomic, assign) NSInteger class_id;

@property (nonatomic, assign) NSInteger test_id;

@property (nonatomic, assign) NSInteger theme_id;

@property (nonatomic, copy) NSString *class_name;

@property (nonatomic, copy) NSString *test_name;

@property (nonatomic, copy) NSString *theme_name;

@property (nonatomic, copy) NSString *date_reg;

@property (nonatomic, copy) NSString *date_reg_temp;

@end
