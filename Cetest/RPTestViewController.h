//
//  RPTestViewController.h
//  Cetest
//
//  Created by Ruslan on 3/16/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPTest.h"
@class Theme_List;
@class RPFullTest;

@interface RPTestViewController : UIViewController

@property (strong, nonatomic) NSArray<Theme_List *> *theme_list;

@property (nonatomic, strong) RPTest *test;

@property (nonatomic, strong) RPFullTest *currentTest;

@end
