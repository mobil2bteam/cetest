//
//  RPLoadDataVC.m
//  Е-класс
//
//  Created by Ruslan on 3/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPLoadDataVC.h"
#import "AppDelegate.h"
#import "Header.h"
#import "Constants.h"

@interface RPLoadDataVC () <VKSdkDelegate, VKSdkUIDelegate>

@property (weak, nonatomic) IBOutlet UIView *testView;
@end

@implementation RPLoadDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[VKSdk initializeWithAppId:kVKIdentifier] registerDelegate:self];
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:kVKIdentifier];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];

    
    if (IS_IPAD) {
        [self loadData];
    } else {
        if ([kUserDefaults valueForKey:@"firstLoad"]) {
            [self loadData];
        } else {
            [kUserDefaults setValue:@(YES) forKey:@"firstLoad"];
            [kUserDefaults synchronize];
            self.testView.hidden = YES;
        }
    }
}

#pragma mark - VK

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self.navigationController.topViewController presentViewController:controller animated:NO completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self];
}

- (void)vkSdkUserAuthorizationFailed{
    [self showMessage:kErrorMessage];
}

#pragma mark - Methods

- (void)loadAllData{
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postGetAllDataOnSuccess:^(RPError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (error) {
            [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                [weakSelf loadAllData];
            }];
        } else {
            UIViewController *VC = [[UIStoryboard storyboardWithName:@"Start" bundle:nil] instantiateInitialViewController];
            APP_DELEGATE.window.rootViewController = VC;
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf loadAllData];
        }];
    }];
}

- (void)loadData{
    if ([self isReachility]) {
        if ([kUserDefaults valueForKey:@"login"]) {
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            if ([kUserDefaults valueForKey:@"socialNetwork"]) {
                [self loginViaSocialNetwork];
            } else {
                NSString *password = [kUserDefaults valueForKey:@"password"];
                NSString *login = [kUserDefaults valueForKey:@"login"];
                NSDictionary *params = @{@"type":@"login",
                                         @"login":login,
                                         @"password":password
                                         };
                kUser.socialNetwork = SocialTypeEmail;
                [self authorizationWithParameters:params];
            }
        }
        else {
            [self loadAllData];
        }
    } else {
        [self showMessage:kConnectionErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [self loadData];
        }];
    }
}

- (void)loginViaSocialNetwork{
    NSString *password = [kUserDefaults valueForKey:@"password"];
    NSString *login = [kUserDefaults valueForKey:@"login"];
    NSDictionary *params = @{@"type":@"login",
                             @"login":login,
                             @"password":password
                             };
    if ([[kUserDefaults valueForKey:@"socialNetwork"] isEqualToString:@"fb"]) {
        if ([FBSDKAccessToken currentAccessToken]){
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, picture.type(large), email"}]
             startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error)
                 {
                     [RPUser saveUserWithFB:result];
                     [self authorizationWithParameters:params];
                 }
                 else
                 {
                     [RPUser logOut];
                     [self loadAllData];
                 }
             }];
            
        } else {
            [RPUser logOut];
            [self loadAllData];
        }
    } else {
        [VKSdk wakeUpSession:@[VK_PER_WALL, VK_PER_EMAIL] completeBlock:^(VKAuthorizationState state, NSError *error) {
            if (state == VKAuthorizationAuthorized) {
                [RPUser saveUserWithVK];
                [self authorizationWithParameters:params];
            } else {
                [RPUser logOut];
                [self loadAllData];
            }
        }];
    }
}

- (void)authorizationWithParameters:(NSDictionary*)params{
    [[RPServerManager sharedManager]postLoginWithParams:params onSuccess:^(RPUserInfo *user_info, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (user_info) {
            [self loadAllData];
        } else {
            [RPUser logOut];
            [self loadAllData];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self loadAllData];
    }];
}
- (IBAction)goButtonPressed:(id)sender {
    [self loadData];
}

@end
