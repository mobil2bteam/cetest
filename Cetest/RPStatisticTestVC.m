//
//  RPStatisticTestVC.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#define kChartItemWidth 60.f

#import "RPStatisticTestVC.h"
#import "PNLineChart.h"
#import "PNChartDelegate.h"
#import "RPUserInfo.h"
#import "PNChart.h"
#import "RPTopResultCollectionViewCell.h"
#import "RPResultCollectionViewCell.h"

@interface RPStatisticTestVC ()

@property (weak, nonatomic) IBOutlet UILabel *testNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *themeNameLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *chartContainerWidthConstraint;

@property (weak, nonatomic) IBOutlet UIView *chartContainer;

@property (strong, nonatomic) PNLineChart *lineChart;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resultsCollectionViewHeightConstraint;

@end

@implementation RPStatisticTestVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.topResultsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPTopResultCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPTopResultCollectionViewCell class])];
    
    [self.resultsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPResultCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPResultCollectionViewCell class])];

    self.navigationItem.title = self.resultsArray[0].test_name;
    
    // add observer to automatic update resultsCollectionView's height
    [self addObserver:self forKeyPath:@"resultsCollectionView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [self setUpControls];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [self.view setNeedsDisplay];
    [self.resultsCollectionView reloadData];
    [self.topResultsCollectionView reloadData];
}


- (void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"resultsCollectionView.contentSize"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:[UIApplication sharedApplication]];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"resultsCollectionView.contentSize"]) {
        CGFloat newHeight = self.resultsCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.resultsCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

#pragma mark - Methods

- (void)setUpControls{
    // устанавливаем значения для лейблов
    self.testNameLabel.text = self.resultsArray[0].test_name;
    self.classNameLabel.text = self.resultsArray[0].class_name;
    if (!self.isTest) {
        self.themeNameLabel.text = self.resultsArray[0].theme_name;
    }
    [self setUpChartContainer];
    [self setUpChart];
}

- (void)setUpChartContainer{
    self.chartContainerWidthConstraint.constant = [self containerWidth];
    [self.view layoutIfNeeded];
}

- (void)setUpChart{
    
    self.lineChart = [[PNLineChart alloc]initWithFrame:self.chartContainer.bounds];
    [self.lineChart.chartData enumerateObjectsUsingBlock:^(PNLineChartData *obj, NSUInteger idx, BOOL *stop) {
        obj.pointLabelColor = [UIColor blackColor];
    }];
    self.lineChart.progressLineColor = [UIColor grayColor];
    self.lineChart.yLabelFormat = @"%1f";
    self.lineChart.xLabelFont = [UIFont fontWithName:@"Helvetica-Light" size:10.0];
    [self.lineChart setXLabels:[self emptyLabels]];
    self.lineChart.xLabelColor = [UIColor blackColor];

    self.lineChart.showGenYLabels = NO;
    self.lineChart.showYGridLines = YES;
    
    self.lineChart.yFixedValueMax = [self maxValue] + 1 ;
    self.lineChart.yFixedValueMin = [self minValue] - 1;

    NSArray *data01Array = [self results];
    
    PNLineChartData *data01 = [PNLineChartData new];
    data01.inflexionPointWidth = 20.f;
    data01.showPointLabel = YES;
    data01.pointLabelFont = [UIFont fontWithName:@"Helvetica-Light" size:9.0];
    data01.itemCount = data01Array.count;
    data01.inflexionPointColor = [UIColor whiteColor];
    data01.inflexionPointStyle = PNLineChartPointStyleCircle;
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [PNLineChartDataItem dataItemWithY:yValue];
    };
    self.lineChart.chartData = @[data01];
    [self.lineChart strokeChart];
    [self.chartContainer addSubview:self.lineChart];
}

#pragma mark - Getters

- (CGFloat)containerWidth{
    if (self.isTest) {
        return self.resultsArray.count * 90;
    }
    return self.resultsArray.count * 50;
}

- (NSArray *)results{
    
    if (self.isTest) {
        NSArray <ResultServerModel *> *tempArray = [self.resultsArray copy];
        
        NSArray *themes = [tempArray valueForKeyPath:@"@distinctUnionOfObjects.theme_name"];
        
        NSMutableArray <NSArray <ResultServerModel *> *> *groupedThemes = [[NSMutableArray alloc]init];
        
        
        for (NSString *theme in themes) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"theme_name = %@", theme];
            
            // получаем массив тестов для данного класса
            NSArray *themeArray = [tempArray filteredArrayUsingPredicate:predicate];
            [groupedThemes addObject:themeArray];
        }
        NSMutableArray *results = [[NSMutableArray alloc]init];
        for (NSArray <ResultServerModel *> *arr in groupedThemes) {
            NSInteger count = 0;
            CGFloat sum = 0;
            for (ResultServerModel *r in arr) {
                count = count + 1;
                sum = sum + r.sp;
            }
            [results addObject:@(sum / count)];
        }
        return results;

    } else {
        NSArray <ResultServerModel *> *tempArray = [self.resultsArray copy];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd.MM.yyyy"];
        NSLocale *ruLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [dateFormatter setLocale:ruLocale];
        
        for (ResultServerModel *result in tempArray) {
            NSDateFormatter *formatter2 = [[NSDateFormatter alloc]init];
            [formatter2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *date = [formatter2 dateFromString:result.date_reg];
            result.date_reg_temp = [dateFormatter stringFromDate:date];
        }
        
        NSArray *dates = [tempArray valueForKeyPath:@"@distinctUnionOfObjects.date_reg_temp"];
        
        NSMutableArray <NSArray <ResultServerModel *> *> *groupedDates = [[NSMutableArray alloc]init];
        
        
        for (NSString *date in dates) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date_reg_temp = %@", date];
            
            // получаем массив тестов для данного класса
            NSArray *dateArray = [tempArray filteredArrayUsingPredicate:predicate];
            [groupedDates addObject:dateArray];
        }
        
        NSMutableArray *results = [[NSMutableArray alloc]init];
        for (NSArray <ResultServerModel *> *arr in groupedDates) {
            NSInteger count = 0;
            CGFloat sum = 0;
            for (ResultServerModel *r in arr) {
                count = count + 1;
                sum = sum + r.sp;
            }
            [results addObject:@(sum / count)];
        }
        return results;
    }
}

- (NSArray *)emptyLabels{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (__unused ResultServerModel *result in self.results) {
        [arr addObject:@" "];
    }
    return [arr copy];
}

- (NSInteger)minValue{
    if (self.resultsArray.count > 2) {
        NSInteger min = self.resultsArray[0].sp;
        for (ResultServerModel *result in self.resultsArray) {
            if (result.sp < min) {
                min = result.sp;
            }
        }
        return min;
    }
    return 0;
}


- (NSInteger)maxValue{
    if (self.resultsArray.count) {
        NSInteger max = self.resultsArray[0].sp;
        for (ResultServerModel *result in self.resultsArray) {
            if (result.sp > max) {
                max = result.sp;
            }
        }
        return max;
    }
    return 10;
}

@end
