//
//  NSString+Category.h
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

+(BOOL)isEmpty:(NSString*)testSting;

- (NSString *)md5;

- (NSString *)transliterate;

- (NSString*) reverseString;

@end
