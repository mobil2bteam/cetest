//
//  RPThemeController.m
//  Cetest
//
//  Created by Ruslan on 3/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPThemeController.h"
#import "Header.h"
#import "Constants.h"
#import "RPRunTestController.h"
#import "Test+CoreDataProperties.h"
#import "RPDeleteViewController.h"
#import "SFProgressCircle.h"
#import "RPStatisticTestVC.h"

#define AnimationDuration 0.6

#define fromColor [UIColor colorWithRed:0.910 green:0.624 blue:0.286 alpha:1]

#define toColor [UIColor colorWithRed:0.322 green:0.835 blue:0.580 alpha:1]

@interface RPThemeController() <SSZipArchiveDelegate, FBSDKSharingDelegate, VKSdkDelegate, VKSdkUIDelegate, RPDeleteTestProtocol>

@property (weak, nonatomic) IBOutlet UILabel *noResultLabel;

@property (weak, nonatomic) IBOutlet UIView *progressViewContainer;

@property (weak, nonatomic) IBOutlet UILabel *testNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *freePeriodLabel;

@property (weak, nonatomic) IBOutlet UILabel *themeNameLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *runTestOnlineButton;

@property (weak, nonatomic) IBOutlet UIButton *saveTestButton;

@property (nonatomic, strong) NSString *testPath;

@property (nonatomic, strong) NSString *pathToSaveDirectory;

@property (nonatomic, strong) RPThemeInfo *themeInfo;

@property (nonatomic, strong) MBProgressHUD *HUG;

//@property (nonatomic, assign) BOOL isTestLoadedOnPhone;

@property (nonatomic, strong) Test *localTest;

@property (weak, nonatomic) IBOutlet UIView *deleteButton;

@property (weak, nonatomic) IBOutlet UIView *deleteTestView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deleteTestViewHeightConstraint;

@property (strong, nonatomic) SFCircleGradientView *progressView;

@property (weak, nonatomic) IBOutlet UILabel *ballLabel;

@property (weak, nonatomic) IBOutlet UILabel *detailBallLabel;

@property (weak, nonatomic) IBOutlet UIView *resultInfoView;

@property (weak, nonatomic) IBOutlet UIView *historyView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *historyHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *freePeriodView;

@end

@implementation RPThemeController

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    self.scrollView.hidden = YES;
    self.navigationItem.title = self.currentTheme.name;
    [self loadTheme];
    
    _progressView = [[SFCircleGradientView alloc] initWithFrame:self.progressViewContainer.bounds];
    [_progressView setLineWidth:6];
    _progressView.progress = 0;
    [_progressView setStartColor:fromColor];
    [_progressView setEndColor:toColor];
    [self.progressViewContainer addSubview:_progressView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[VKSdk initializeWithAppId:kVKIdentifier] registerDelegate:self];
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:kVKIdentifier];
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.saveTestButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.saveTestButton.titleLabel.numberOfLines = 2;
    self.saveTestButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.runTestOnlineButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.runTestOnlineButton.titleLabel.numberOfLines = 2;
    self.runTestOnlineButton.titleLabel.textAlignment = NSTextAlignmentCenter;
}

#pragma mark - Getters

- (NSString *)pathToSaveDirectory{
    NSInteger classID = self.themeInfo.info.class_id;
    NSInteger testID = self.themeInfo.info.ID;
    NSInteger themeID = self.currentTheme.ID;
    NSInteger userID = kUser.ID;
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [NSString stringWithFormat:@"%@/%ld/%ld/%ld/%ld", documentsDirectory, (long)userID, (long)classID, (long)testID, (long)themeID];
    return path;
}

- (Test *)localTest{
    NSInteger classID = self.themeInfo.info.class_id;
    NSInteger testID = self.themeInfo.info.ID;
    NSInteger themeID = self.currentTheme.ID;
    NSInteger userID = kUser.ID;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"classID == %@ && testID == %@ && themeID == %@ && userID == %@", @(classID), @(testID), @(themeID), @(userID)];
    NSArray *tests = [Test MR_findAllWithPredicate:predicate];
    if (tests.count) {
        return tests[0];
    } else return nil;
}

#pragma mark - Actions

- (IBAction)backBarButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveTestButtonPressed:(id)sender {
    if (self.localTest) {
        [self openTestOnPhone];
    } else {
        self.HUG = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.HUG.labelText = @"Идет загрузка теста";
        self.HUG.mode = MBProgressHUDModeDeterminate;
        
        NSError * error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:self.pathToSaveDirectory
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        if (error != nil) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self showMessage:kErrorMessage];
            return;
        }
        __weak typeof(self) weakSelf = self;
        NSString *archive = [NSString stringWithFormat:@"%@%@", @"http://eclass.cetest.ru/sc/", self.themeInfo.subject.url_file];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        NSURL *URL = [NSURL URLWithString:archive];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
            
            weakSelf.HUG.progress = downloadProgress.fractionCompleted;
            
        } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
            return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            if (error) {
                [weakSelf.HUG hide:YES];
                [weakSelf showMessage:kErrorMessage];
            } else {
//                weakSelf.testPath = [[response suggestedFilename]componentsSeparatedByString:@"."][0];
                weakSelf.testPath =  [[response suggestedFilename] stringByReplacingOccurrencesOfString:@".zip" withString:@""];
                weakSelf.HUG.labelText = @"Идет распаковка теста";
                weakSelf.HUG.progress = 0;
                [weakSelf UnzippingWithResponse:response];
            }
        }];
        [downloadTask resume];
    }
}

- (IBAction)runTestOnlineButtonPressed:(id)sender{
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"runTestNavVC"];
    RPRunTestController *runTestController = navController.viewControllers[0];
    runTestController.isLocalLink = NO;
    runTestController.link = self.themeInfo.subject.url_web;
    runTestController.themeID = self.currentTheme.ID;
    [self presentViewController:navController animated:YES completion:nil];
}

- (IBAction)deleteTestButtonPressed:(id)sender{
    RPDeleteViewController *shareController = [[RPDeleteViewController alloc] initWithNibName: @"RPDeleteViewController" bundle: nil];
    shareController.delegate = self;
    shareController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    shareController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:shareController animated:YES completion:nil];
}

#pragma mark - RPDeleteTestProtocol

- (void)deleteTest{
    // удаляем папку с файлами
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",documentsPath, self.localTest.userID, self.localTest.classID, self.localTest.testID, self.localTest.themeID];
    NSError *error;
    [fileManager removeItemAtPath:path error:&error];
    // удаляем данные с БД
    NSInteger classID = self.themeInfo.info.class_id;
    NSInteger testID = self.themeInfo.info.ID;
    NSInteger themeID = self.currentTheme.ID;
    NSInteger userID = kUser.ID;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"classID == %@ && testID == %@ && themeID == %@ && userID == %@", @(classID), @(testID), @(themeID), @(userID)];
    NSArray *tests = [Test MR_findAllWithPredicate:predicate];
    for (Test *test in tests) {
        [test MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
    // обновляем тему
    [self loadTheme];
}

#pragma mark - Methods

- (void)UnzippingWithResponse:(NSURLResponse*)response {
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *outputPath = [NSString stringWithFormat:@"%@/%@", self.pathToSaveDirectory, self.testPath];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,[response suggestedFilename]];
    [SSZipArchive unzipFileAtPath:filePath toDestination:outputPath delegate:self];
}

- (void)zipArchiveProgressEvent:(unsigned long long)loaded total:(unsigned long long)total{
    self.HUG.progress = (loaded * 1.0)/total;
    if (loaded == total) {
        [self removeFile:[NSString stringWithFormat:@"%@.zip", self.testPath]];
    }
}

- (void)openTestOnPhone{
    UINavigationController *navController = [self.storyboard instantiateViewControllerWithIdentifier:@"runTestNavVC"];
    RPRunTestController *newController = navController.viewControllers[0];
    newController.isLocalLink = YES;
    newController.localTest = self.localTest;
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)saveToDataBase{
    NSInteger classID = self.themeInfo.info.class_id;
    NSInteger testID = self.themeInfo.info.ID;
    NSInteger themeID = self.currentTheme.ID;
    
    Test *newTest = [Test MR_createEntity];
    newTest.name = self.themeInfo.info.test_name;
    newTest.classID = @(classID);
    newTest.testID = @(testID);
    newTest.themeID = @(themeID);
    newTest.filePath = self.testPath;
    newTest.userID = @(kUser.ID);
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            [self.HUG hide:YES];
            [self openTestOnPhone];
        } else if (error) {
            [self.HUG hide:YES];
            [self showMessage:kErrorMessage];
        }
    }];
}

- (void)removeFile:(NSString *)fileName
{
    // удаляем архив
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:fileName];
    NSError *error;
    [fileManager removeItemAtPath:filePath error:&error];
    // сохраняем запись в БД
    [self saveToDataBase];
}

- (NSString *)documentDirectory{
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)prepareView{
    [self prepareTestPeriodView];
    if (!self.themeInfo.subject.url_file.length) {
        [self.saveTestButton removeFromSuperview];
    }
    // результаты
    if (![APP_DELEGATE.user_info resultsByClass:self.themeInfo.info.class_name andTest:self.themeInfo.info.test_name andTheme:self.themeInfo.subject.name].count) {
        self.historyView.hidden = YES;
        self.historyHeightConstraint.constant = 0;
    } else {
        self.historyView.hidden = NO;
        self.historyHeightConstraint.constant = 70;
    }
    
    // если тема уже скачивалась на телефон, то меняем надпись на кнопке
    if (self.localTest) {
        [self.saveTestButton setTitle:@"Запустить с телефона" forState:UIControlStateNormal];
        self.deleteTestViewHeightConstraint.constant = 70.0;
        self.deleteTestView.hidden = NO;
    } else {
        self.deleteTestViewHeightConstraint.constant = 0.0;
        self.deleteTestView.hidden = YES;
        [self.saveTestButton setTitle:@"Загрузить на телефон" forState:UIControlStateNormal];
    }
    
    //заполняем вью данными
    self.testNameLabel.text = self.themeInfo.info.discipline_name;
    self.classNameLabel.text = self.themeInfo.info.class_name;
    self.themeNameLabel.text = self.themeInfo.subject.name;
    
    //если пользователь не авторизован, то разрешаем только запуск теста онлайн
    if (!kUser.isLogin) {
        [self.saveTestButton removeFromSuperview];
        self.deleteTestViewHeightConstraint.constant = 0.0;
        self.deleteTestView.hidden = YES;
    }

    if (self.themeInfo.subject.sp) {
        self.noResultLabel.hidden = YES;
        self.resultInfoView.hidden = NO;
        self.ballLabel.text = [NSString stringWithFormat:@"%ld%c", (long)self.themeInfo.subject.procent, '%'];
        self.detailBallLabel.text = [NSString stringWithFormat:@"%ld / %ld", (long)self.themeInfo.subject.sp, (long)self.themeInfo.subject.tp];
        [_progressView setProgress:(self.themeInfo.subject.sp * 1.f) / self.themeInfo.subject.tp  animateWithDuration:0.5];
    } else {
        self.noResultLabel.hidden = NO;
        self.resultInfoView.hidden = YES;
    }
    self.scrollView.hidden = NO;
    
    [self.view layoutIfNeeded];
}

//- (void)prepareTestPeriodView{
//    // если тест свободный и тестовый период не истек, то
//    if ((self.themeInfo.options.free_open || self.themeInfo.options.free_test) && self.themeInfo.options.free_day_count) {
//        if (self.themeInfo.options.free_day_count == 1) {
//            self.freePeriodLabel.text = [NSString stringWithFormat:@"Тестовый период остался 1 день"];
//        } else {
//            self.freePeriodLabel.text = [NSString stringWithFormat:@"Тестовый период осталось %ld дня", (long)self.themeInfo.options.free_day_count];
//        }
//    } else if (self.themeInfo.options.buy) {
//        // если тест куплен
//        self.freePeriodView.backgroundColor = kSuccessGreenColor;
//        self.freePeriodLabel.text = @"Тест куплен";
//    } else {
//        [self.freePeriodView removeFromSuperview];
//    }
//}

- (void)prepareTestPeriodView{
    if (self.themeInfo.options.buy) {
        // если тест куплен
        self.freePeriodView.backgroundColor = kSuccessGreenColor;
        self.freePeriodLabel.text = @"Тест куплен";
    } else if ((self.themeInfo.options.free_open || self.themeInfo.options.free_test) && self.themeInfo.options.free_day_count) {
        // если тест свободный и тестовый период не истек, то
        if (self.themeInfo.options.free_day_count == 1) {
            self.freePeriodLabel.text = [NSString stringWithFormat:@"Тестовый период остался 1 день"];
        } else {
            self.freePeriodLabel.text = [NSString stringWithFormat:@"Тестовый период осталось %ld дня", (long)self.themeInfo.options.free_day_count];
        }
    } else {
        [self.freePeriodView removeFromSuperview];
    }
}

- (void)loadTheme{
    self.themeInfo = nil;
    self.scrollView.hidden = YES;
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[RPServerManager sharedManager]postGetThemeWithID:self.currentTheme.ID onSuccess:^(RPError *error, RPThemeInfo *themeInfo) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        if (error) {
            [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                [weakSelf loadTheme];
            } andBackHandler:^(UIAlertAction *action) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            weakSelf.themeInfo = themeInfo;
            [weakSelf prepareView];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf loadTheme];
        } andBackHandler:^(UIAlertAction *action) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }];
}

- (void)loadTheWithData{
    self.themeInfo = nil;
    self.scrollView.hidden = YES;
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[RPServerManager sharedManager]postGetThemeWithID:self.currentTheme.ID onSuccess:^(RPError *error, RPThemeInfo *themeInfo) {
        if (error) {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                [weakSelf loadTheWithData];
            } andBackHandler:^(UIAlertAction *action) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            weakSelf.themeInfo = themeInfo;
            [weakSelf authorization];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf loadTheWithData];
        } andBackHandler:^(UIAlertAction *action) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }];
}

//- (void)loadAllData{
//    __weak typeof(self) weakSelf = self;
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    [[RPServerManager sharedManager]postGetAllDataOnSuccess:^(RPError *error) {
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        if (error) {
//            [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
//                [weakSelf loadAllData];
//            }];
//        } else {
//            [weakSelf prepareView];
//        }
//    } onFailure:^(NSError *error, NSInteger statusCode) {
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//        [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
//            [weakSelf loadAllData];
//        }];
//    }];
//}

- (void)authorization{
    NSString *password = [kUserDefaults valueForKey:@"password"];
    NSString *login = [kUserDefaults valueForKey:@"login"];
    NSDictionary *params = @{@"type":@"login",
                             @"login":login,
                             @"password":password
                             };
    __weak typeof(self) weakSelf = self;
    [[RPServerManager sharedManager]postLoginWithParams:params onSuccess:^(RPUserInfo *user_info, RPError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [weakSelf prepareView];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [weakSelf prepareView];
    }];
}

#pragma mark - RPShareResultProtocol

- (void)prepareShareViaFB{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [self showFaceBookShareDialog];
    } else {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login
         logInWithReadPermissions: @[@"public_profile", @"email"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if (error) {
                 [self showMessage:kErrorMessage];
             } else if (!result.isCancelled) {
                 [self showFaceBookShareDialog];
             }
         }];
    }
}

- (void)prepareShareViaVK{
    // если пользователь вошел не через ВК, то просим его войти
    if (kUser.socialNetwork != SocialTypeVK){
        [VKSdk authorize:@[VK_PER_WALL, VK_PER_EMAIL]];
    } else {
        [self shareViaVK];
    }
}

- (void)shareViaVK{
    VKShareDialogController * shareDialog = [VKShareDialogController new];
    shareDialog.text = @"Результат";
    NSString *link = [NSString stringWithFormat:@"%@myresult.php?q=%@", kAddressSite, self.themeInfo.subject.result_key];
    shareDialog.shareLink = [[VKShareLink alloc] initWithTitle:@"Cetest" link:[NSURL URLWithString:link]];
    shareDialog.requestedScope = [[VKSdk accessToken] permissions]; // передаём права явно
    [shareDialog setCompletionHandler:^(VKShareDialogController *dialogController, VKShareDialogControllerResult result) {
        if (kUser.socialNetwork != SocialTypeVK) {
            [VKSdk forceLogout];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [self presentViewController:shareDialog animated:NO completion:nil];
}

#pragma mark - VK

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{
    if (result.token){
        [self shareViaVK];
    }
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self];
}

- (void)vkSdkUserAuthorizationFailed{
    [self showMessage:kErrorMessage];
}

#pragma mark - FBSDKSharingDelegate

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results{
    [self logOutViaFaceBook];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error{
    [self logOutViaFaceBook];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer{
    [self logOutViaFaceBook];
}

- (void)logOutViaFaceBook{
    if (kUser.socialNetwork != SocialTypeFaceBook) {
        [[FBSDKLoginManager new] logOut];
    }
}

- (void)showFaceBookShareDialog{
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.mode = FBSDKShareDialogModeAutomatic;
    dialog.fromViewController = self;
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc]init];
    content.contentTitle = @"Cetest";
    content.contentDescription = @"Результат";
    NSString *link = [NSString stringWithFormat:@"%@myresult.php?q=%@", kAddressSite, self.themeInfo.subject.result_key];
    content.contentURL = [NSURL URLWithString:link];
    dialog.shareContent = content;
    dialog.delegate = self;
    if (![dialog canShow]) {
        dialog.mode = FBSDKShareDialogModeFeedBrowser;
    }
    [dialog show];
}

- (IBAction)historyButtonPressed:(id)sender {
    RPStatisticTestVC *vc = [[UIStoryboard storyboardWithName:@"Result" bundle:nil]instantiateViewControllerWithIdentifier:@"RPStatisticTestVC"];
    vc.resultsArray = [APP_DELEGATE.user_info resultsByClass:self.themeInfo.info.class_name andTest:self.themeInfo.info.test_name andTheme:self.themeInfo.subject.name];;
    vc.isTest = NO;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Unwind segue

-(IBAction)prepareForThemeUnwind:(UIStoryboardSegue *)segue {
    [self loadTheme];
}

@end
