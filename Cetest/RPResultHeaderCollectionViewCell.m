//
//  RPResultHeaderCollectionViewCell.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPResultHeaderCollectionViewCell.h"

@implementation RPResultHeaderCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)headerButtonPressed:(id)sender {
    if (self.callback) {
        self.callback(self.section);
    }
}

@end
