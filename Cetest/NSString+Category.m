//
//  NSString+Category.m
//  DrinkAndDrive Driver
//
//  Created by Ruslan on 1/15/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "NSString+Category.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (Category)
- (NSString *)md5
{
    const char* cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (int)strlen(cStr), result);
    
    static const char HexEncodeChars[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    char *resultData = malloc(CC_MD5_DIGEST_LENGTH * 2 + 1);
    
    for (uint index = 0; index < CC_MD5_DIGEST_LENGTH; index++) {
        resultData[index * 2] = HexEncodeChars[(result[index] >> 4)];
        resultData[index * 2 + 1] = HexEncodeChars[(result[index] % 0x10)];
    }
    resultData[CC_MD5_DIGEST_LENGTH * 2] = 0;
    
    NSString *resultString = [NSString stringWithCString:resultData encoding:NSASCIIStringEncoding];
    free(resultData);
    
    return resultString;
}

- (NSString *)transliterate{
    NSMutableString *encoded = [NSMutableString stringWithCapacity:3*[self length]];
    const char *characters = [self UTF8String];
    NSUInteger length = strlen(characters);
    for (NSUInteger i = 0; i < length; ++i) {
        char character = characters[i];
        int left = character & 0xF;
        int right = (character >> 4) & 0xF;
        [encoded appendFormat:@"%c%X%X",'%', right, left];
    }
    return encoded;
}

+(BOOL)isEmpty:(NSString*)testSting{
    if (testSting == nil) {
        return YES;
    }
    if ([testSting isEqualToString:@""]) {
        return YES;
    }
    if (testSting.length==0) {
        return YES;
    }
    return NO;
}

- (NSString*) reverseString
{
    NSMutableString *reversedStr;
    NSInteger len = [self length];
    
    reversedStr = [NSMutableString stringWithCapacity:len];
    
    while ( len > 0 )
        [reversedStr appendString:[NSString stringWithFormat:@"%C",[self characterAtIndex:--len]]];
    
    return reversedStr;
}

@end
