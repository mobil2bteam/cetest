//
//  RPSigInViewController.h
//  Е-класс
//
//  Created by Ruslan on 9/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPSignInProtocol.h"

@interface RPSigInViewController : UIViewController

@property (nonatomic, strong) id <RPSignInProtocol> delegate;

@end
