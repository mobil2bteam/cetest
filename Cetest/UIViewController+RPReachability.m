//
//  UIViewController+RPReachability.m
//  Cetest
//
//  Created by Ruslan on 3/27/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "UIViewController+RPReachability.h"
#import "Reachability.h"

@implementation UIViewController (RPReachability)

- (BOOL)isReachility {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    } else {
        return YES;
    }
}

@end
