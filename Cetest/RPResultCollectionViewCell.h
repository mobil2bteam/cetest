//
//  RPResultCollectionViewCell.h
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPResultCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *ballLabel;

@property (weak, nonatomic) IBOutlet UILabel *themeLabel;

@property (weak, nonatomic) IBOutlet UILabel *bestResultLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
