//
//  RPStatisticTestVC.h
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ResultServerModel;

@interface RPStatisticTestVC : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *resultsCollectionView;

@property (strong, nonatomic) NSArray <ResultServerModel *> *resultsArray;

@property (weak, nonatomic) IBOutlet UICollectionView *topResultsCollectionView;

@property (assign, nonatomic) BOOL isTest;

- (NSInteger)maxValue;

- (NSArray *)results;

@end
