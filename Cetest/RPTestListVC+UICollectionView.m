//
//  RPViewController+UICollectionView.m
//  Е-класс
//
//  Created by Ruslan on 5/1/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPTestListVC+UICollectionView.h"
#import "RPClassCollectionViewCell.h"
#import "RPTestCollectionViewCell.h"
#import "RPTestCollectionReusableView.h"
#import "RPClassList.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "RPTest.h"

@implementation RPTestListVC (UICollectionView)

#pragma mark - Collection view delegates

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (collectionView == self.classCollectionView) {
        return 1;
    }
    // if is all tests selected
    if (!self.isUserTests && self.allTestArray.count) {
        // если нет тестов, то показываем вью
        self.noTestsView.hidden = self.selectedTestArray.count;
        return self.selectedTestArray.count;
    }
    // if is user tests selected
    if (self.isUserTests) {
        // if free test is avaliable user tests selected
        if (self.freeTestArray.count) {
            return self.buyTestArray.count + 1;
        } else {
            return self.buyTestArray.count;
        }
    }
    return 0;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.classCollectionView) {
        return APP_DELEGATE.class_list.count + 1;
    }
    //    [collectionView.collectionViewLayout invalidateLayout];
    if (!self.isUserTests && self.allTestArray.count) {
        return self.selectedTestArray[section].count;
    }
    // if is user tests selected
    if (self.isUserTests) {
        if (!section && self.freeTestArray.count) {
            return self.freeTestArray.count;
        }
        if (self.freeTestArray.count) {
            return self.buyTestArray[section - 1].count;
        } else {
            return self.buyTestArray[section].count;
        }
    }
    return 0;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.classCollectionView) {
        RPClassCollectionViewCell *cell = (RPClassCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPClassCollectionViewCell class]) forIndexPath:indexPath];
        if (!indexPath.row) {
            cell.classLabel.text = kAllClassedKey;
        } else {
            cell.classLabel.text = APP_DELEGATE.class_list[indexPath.row - 1].name;
        }
        if ([cell.classLabel.text isEqualToString:[kUserDefaults valueForKey:kSelectedClassKey]]) {
            [cell setSelected:YES];
        } else {
            [cell setSelected:NO];
        }
        return cell;
    }
    RPTestCollectionViewCell *cell = (RPTestCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPTestCollectionViewCell class]) forIndexPath:indexPath];
    
    // для всех тестов
    if (!self.isUserTests) {
        [cell configureCellWithTest:self.selectedTestArray forIndexPath:indexPath];
    }
    
    // для тестов пользователя
    if (self.isUserTests) {
        if (self.freeTestArray.count) {
            if (!indexPath.section) {
                [cell configureCellWithFreeTest:self.freeTestArray forIndexPath:indexPath];
            } else {
                NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section - 1];
                [cell configureCellWithUserTest:self.buyTestArray forIndexPath:newIndexPath];
            }
        } else {
            [cell configureCellWithUserTest:self.buyTestArray forIndexPath:indexPath];
        }
    }
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.classCollectionView) {
        return nil;
    }
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPTestCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPTestCollectionReusableView class]) forIndexPath:indexPath];
        // для всех тестов
        if (!self.isUserTests) {
            NSString *className = self.selectedTestArray[indexPath.section][0].class_name;
            headerView.classLabel.text = className;
            headerView.classLabel.hidden = headerView.buyAllTestButton.hidden = NO;
            NSString *title = [NSString stringWithFormat:@"Купить весь %@ за %ld рублей",className, (long)[RPClassList priceForClass:className]];
            headerView.buyAllTestButton.tag = indexPath.section;
            [headerView.buyAllTestButton setTitle:title forState:UIControlStateNormal];
        } else {
            // для тестов пользователя
            headerView.buyAllTestButton.hidden = YES;
            [headerView layoutIfNeeded];
            if (!indexPath.section) {
                if (self.freeTestArray.count) {
                    headerView.classLabel.text = @"Бесплатные тесты";
                } else {
                    headerView.classLabel.text = self.buyTestArray[0][0].class_name;
                }
            } else {
                if (self.freeTestArray.count) {
                    headerView.classLabel.text = self.buyTestArray[indexPath.section - 1][0].class_name;
                } else {
                    headerView.classLabel.text = self.buyTestArray[indexPath.section][0].class_name;
                }
            }
        }
        reusableview = headerView;
    }
    return reusableview;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (collectionView == self.classCollectionView) {
        return CGSizeZero;
    }
    //если выбраны Все классы
    if (!self.isUserTests) {
        // цена за целый класс, если цена == 0, то не показываем кнопку  [Купить класс]
        NSInteger price = (long)[RPClassList priceForClass:self.selectedTestArray[section][0].class_name];
        if ([self hasUserBoughtClassForSection:section] || !price) {
            return CGSizeMake(0, 37);
        }
        return CGSizeMake(0, 80);
    }
    if (self.isUserTests) {
        return CGSizeMake(0, 37);
    }
    return CGSizeMake(0, 37);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.classCollectionView) {
        
        if (indexPath.row == 0) {
            self.selectedClass = kAllClassedKey;
            [kUserDefaults setValue:kAllClassedKey forKey:kSelectedClassKey];
            [kUserDefaults synchronize];
        } else {
            self.selectedClass = APP_DELEGATE.class_list[indexPath.row - 1].name;
            [kUserDefaults setValue:self.selectedClass forKey:kSelectedClassKey];
            [kUserDefaults synchronize];
        }
        self.selectedTestArray = nil;
        [self.classCollectionView reloadData];
        [self.classCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [self.testCollectionView reloadData];
        
    }
}
#pragma mark  - Collection view cell layout / size

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.classCollectionView) {
        return CGSizeMake(100, 36);
    }
    CGFloat height, width, testCollectionViewHeight;
    testCollectionViewHeight = CGRectGetHeight(self.testCollectionView.bounds);
    
    if (IS_IPAD)
    {
        if (kIsLandscapeOrientation) {
            width = (collectionView.frame.size.width - 50)/4.0;
            height = (testCollectionViewHeight-20-80-4)/2.0;

        } else {
            width = (collectionView.frame.size.width - 40)/3.0;
            height = (testCollectionViewHeight-30-80-4)/3.0;
        }
    }
    else {
//        if (kIsIphone4) {
//            width = (collectionView.frame.size.width-24)/2.0;
//            height = (testCollectionViewHeight-40-4)/2.0;
//        } else {
            width = (collectionView.frame.size.width - 20 - 10)/2.0;
            height = 200;
//        }
    }
    return CGSizeMake(width, height);
}

#pragma mark - Collection view cell paddings

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(2, 10, 2, 10); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

@end
