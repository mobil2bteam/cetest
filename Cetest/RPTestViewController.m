//
//  RPTestViewController.m
//  Cetest
//
//  Created by Ruslan on 3/16/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//
#import "Header.h"
#import "Constants.h"
#import "RPServerManager.h"
#import "RPTestViewController.h"
#import "RPPromoCodeViewController.h"
#import "RPThemeController.h"
#import "RPStatisticTestVC.h"
#import "RPTestBoughtController.h"
#import <StoreKit/StoreKit.h>
#import "RPSigInViewController.h"

@interface RPTestViewController () < SKProductsRequestDelegate, SKPaymentTransactionObserver, RPSignInProtocol>

@property (weak, nonatomic) IBOutlet UIView *freePeriodView;

@property (weak, nonatomic) IBOutlet UILabel *freePeriodLabel;

@property (weak, nonatomic) IBOutlet UIButton *buyTestButton;

@property (weak, nonatomic) IBOutlet UIButton *promoCodeButton;

@property (weak, nonatomic) IBOutlet UILabel *testNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *testClassLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *testBackgroundImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@property (nonatomic, assign) BOOL showPromoCodeView;

@property (nonatomic, assign) BOOL isVariant1;

@property (weak, nonatomic) IBOutlet UIButton *variant1Button;

@property (weak, nonatomic) IBOutlet UIButton *variant2Button;

@property (weak, nonatomic) IBOutlet UIButton *statisticButton;

@end

@implementation RPTestViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton *btn = [[UIButton alloc]init];
    for (UIButton *b in @[self.variant1Button, self.variant2Button]) {
        [b setTitleColor:btn.tintColor forState:UIControlStateHighlighted];
        [b setTitleColor:btn.tintColor forState:UIControlStateSelected];
        [b setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    self.isVariant1 = YES;
    self.showPromoCodeView = NO;
    self.scrollView.hidden = YES;
    self.navigationItem.title = self.test.discipline_name;
    // add observer to automatic update tableView's height
    [self addObserver:self forKeyPath:@"tableView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [self loadFullTest];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
    if (selectedIndexPath) {
        [self.tableView deselectRowAtIndexPath:selectedIndexPath animated:NO];
    }
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    self.buyTestButton.layer.masksToBounds = YES;
    self.buyTestButton.layer.cornerRadius = 3.0;
    for (UIButton *b in @[self.promoCodeButton, self.statisticButton]) {
        b.layer.cornerRadius = 3.0;
        b.layer.borderColor = [UIColor blackColor].CGColor;
        b.layer.borderWidth = 1.0;
        b.layer.masksToBounds = YES;
    }
}

- (void)dealloc{
    if ([self observationInfo]) {
        [self removeObserver:self forKeyPath:@"tableView.contentSize"];
    }
    [[SKPaymentQueue defaultQueue]removeTransactionObserver:self];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"tableView.contentSize"]) {
        CGFloat newHeight = self.tableView.contentSize.height;
        self.tableViewHeightConstraint.constant = newHeight;
            [self.view layoutIfNeeded];
    }
}

#pragma mark - Methods

// возвращает массив тем по вариантам - 0 и 1 - это первый вариант, 2 - второй
- (NSArray<Theme_List *> *)theme_list{
    if (_theme_list) {
        return _theme_list;
    }
    NSMutableArray <Theme_List *> *theme_list = [[NSMutableArray alloc]init];
    for (Theme_List *theme in self.currentTest.theme_list) {
        if (self.isVariant1) {
            if (theme.variant == 0 || theme.variant == 1) {
                [theme_list addObject:theme];
            }
        } else {
            if (theme.variant == 2) {
                [theme_list addObject:theme];
            }
        }
    }
    return [theme_list copy];
}

#pragma mark - View preparation

- (void)prepareView{
    [self prepareTestPeriodView];

    // устанавливаем информацию о тесте
    self.testNameLabel.text = self.currentTest.info.discipline_name;
    self.testClassLabel.text = self.currentTest.info.class_name;
    
    // загружаем иконку теста
    [self.testBackgroundImageView RP_setImageWithURL:[NSString stringWithFormat:@"%@%@", @"http://eclass.cetest.ru/sc/", self.currentTest.info.discipline_icon_url]];
   
    // скрываем промо код, если пришел такой ответ от сервера
    if (!self.showPromoCodeView) {
        self.promoCodeButton.hidden = YES;
    }
    
    // если тест куплен
    if (self.currentTest.options.buy) {
        // скрываем кнопку Купить и кнопку Промо код
        self.buyTestButton.hidden = YES;
        self.promoCodeButton.hidden = YES;
        
//        // если есть результаты для данного теста, то отображаем кнопку прогресса
//        if ([APP_DELEGATE.user_info resultsByClass:self.currentTest.info.class_name andTest:self.currentTest.info.test_name].count) {
//            self.statisticButton.hidden = NO;
//        }
    } else {
        // если тест не куплен, то устанавливаем цену для покупки
        [self.buyTestButton setTitle:[NSString stringWithFormat:@"Купить за %ld рублей", (long)self.currentTest.info.price] forState:UIControlStateNormal];
    }
    
    [self.tableView reloadData];
    self.scrollView.hidden = NO;
    // если есть сообщение и пользователь раньше не запрещал показывать его, то показываем сообщение о тестовом периоде
    if (!self.currentTest.options.buy && self.currentTest.message && ![kUserDefaults valueForKey:kHideFreeTestInfoKey]) {
        [self showFreeTestInfoAlert];
    }
}

- (void)prepareTestPeriodView{
    if (self.currentTest.options.buy) {
        // если тест куплен
        self.freePeriodView.backgroundColor = kSuccessGreenColor;
        self.freePeriodLabel.text = @"Тест куплен";
    } else if ((self.currentTest.options.free_open || self.currentTest.options.free_test) && self.currentTest.options.free_day_count) {
        // если тест свободный и тестовый период не истек, то
        if (self.currentTest.options.free_day_count == 1) {
            self.freePeriodLabel.text = [NSString stringWithFormat:@"Тестовый период остался 1 день"];
        } else {
            self.freePeriodLabel.text = [NSString stringWithFormat:@"Тестовый период осталось %ld дня", (long)self.currentTest.options.free_day_count];
        }
    } else {
        [self.freePeriodView removeFromSuperview];
    }
}

- (void)loadFullTest{
    self.currentTest = nil;
    __weak typeof(self) weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:weakSelf.view animated:YES];
    [[RPServerManager sharedManager]postShowPromoCodeOnSuccess:^(RPError *error, BOOL show) {
        if (error) {
            [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
            [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                [weakSelf loadFullTest];
            } andBackHandler:^(UIAlertAction *action) {
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }];
        } else {
            weakSelf.showPromoCodeView = show;
            [[RPServerManager sharedManager]postGetFullTestWithTestID:weakSelf.test.ID onSuccess:^(RPFullTest *test, RPError *error) {
                [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                if (error) {
                    [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                        [weakSelf loadFullTest];
                    } andBackHandler:^(UIAlertAction *action) {
                        [weakSelf.navigationController popViewControllerAnimated:YES];
                    }];
                } else {
                    weakSelf.currentTest = test;
                    [weakSelf prepareView];
                }
            } onFailure:^(NSError *error, NSInteger statusCode) {
                [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
                [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
                    [weakSelf loadFullTest];
                } andBackHandler:^(UIAlertAction *action) {
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }];
            }];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf loadFullTest];
        } andBackHandler:^(UIAlertAction *action) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    }];
}

- (void)showFreeTestInfoAlert{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:self.currentTest.message.text
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:nil];
    UIAlertAction *hideAction = [UIAlertAction
                                   actionWithTitle:@"Больше не показывать"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [kUserDefaults setValue:@"hide" forKey:kHideFreeTestInfoKey];
                                       [kUserDefaults synchronize];
                                   }];
    [alertController addAction:hideAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - RPSignInProtocol

- (void)authorization{
    [self.navigationController.tabBarController setSelectedIndex:2];
}

- (void)registration{
    [self.navigationController.tabBarController setSelectedIndex:2];
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if ([identifier isEqualToString:@"promoCodeSegue"]) {
        if (!kUser.isLogin) {
            RPSigInViewController *shareController = [[RPSigInViewController alloc] initWithNibName: @"RPSigInViewController" bundle: nil];
            shareController.delegate = self;
            shareController.modalPresentationStyle = UIModalPresentationOverFullScreen;
            shareController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:shareController animated:YES completion:nil];
        }
        return kUser.isLogin;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"promoCodeSegue"]) {
        RPPromoCodeViewController *newController = segue.destinationViewController;
        newController.currentTest = self.currentTest;
    }
    if ([segue.identifier isEqualToString:@"themeOpenSegue"]) {
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        Theme_List *currentTheme = [self theme_list][selectedIndexPath.row];
        RPThemeController *newController = segue.destinationViewController;
        newController.currentTheme = currentTheme;
    }
    if ([segue.identifier isEqualToString:@"testBoughtSegue"]) {
        RPTestBoughtController *newController = segue.destinationViewController;
        newController.currentTest = self.currentTest;
    }
}

#pragma mark - Actions 

- (IBAction)variant1ButtonPressed:(id)sender {
    if (!self.variant1Button.isSelected) {
        self.variant1Button.selected = YES;
        self.variant2Button.selected = NO;
        self.isVariant1 = YES;
        self.theme_list = nil;
        [self.tableView reloadData];
    }
}

- (IBAction)variant2ButtonPressed:(id)sender {
    if (!self.variant2Button.isSelected) {
        self.variant2Button.selected = YES;
        self.variant1Button.selected = NO;
        self.isVariant1 = NO;
        self.theme_list = nil;
        [self.tableView reloadData];
    }
}

- (IBAction)statisticButtonPressed:(id)sender {
    RPStatisticTestVC *vc = [[UIStoryboard storyboardWithName:@"Result" bundle:nil]instantiateViewControllerWithIdentifier:@"RPStatisticTestVC"];
    vc.resultsArray = [APP_DELEGATE.user_info resultsByClass:self.currentTest.info.class_name andTest:self.currentTest.info.test_name];;
    vc.isTest = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)buyTestButtonPressed:(id)sender {
    if ([kUser isLogin]) {
        __weak typeof(self) weakSelf = self;
        NSString *message = [NSString stringWithFormat:@"Вы действительно хотите купить тест %@ за %ld руб.", self.currentTest.info.class_name, (long)self.currentTest.info.price];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Купить класс"
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Купить"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [weakSelf prepareBuyTest];
                                   }];
        UIAlertAction *restoreAction = [UIAlertAction
                                        actionWithTitle:@"Я уже покупал"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [weakSelf restore];
                                        }];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Отменить"
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        [alertController addAction:okAction];
        [alertController addAction:restoreAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        RPSigInViewController *shareController = [[RPSigInViewController alloc] initWithNibName: @"RPSigInViewController" bundle: nil];
        shareController.delegate = self;
        shareController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        shareController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:shareController animated:YES completion:nil];
    }
}

#pragma mark - In-App

- (void)prepareBuyTest{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if([SKPaymentQueue canMakePayments]){
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:_currentTest.info.key_ios]];
        productsRequest.delegate = self;
        [productsRequest start];
    }
    else{
        [self showMessage:@"Покупки недоступны"];
    }
}

- (void)buyTest{
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postBuyTestWithID:weakSelf.currentTest.info.ID promoCode:nil onSuccess:^(RPError *error, RPResult *result) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        if (error) {
            [weakSelf showMessage:error.msg];
        } else {
            if ([result.result isEqualToString:@"ok"]) {
                if (kUser.isLogin) {
                    [weakSelf performSegueWithIdentifier:@"testBoughtSegue" sender:self];
                } else {
                    [weakSelf performSegueWithIdentifier:@"testBoughtSegue" sender:self];
                }
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        [weakSelf showMessage:kErrorMessage];
    }];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        SKPayment *payment = [SKPayment paymentWithProduct:validProduct];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else if(!validProduct){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:@"Нет доступных продуктов"];
    }
}

- (IBAction) restore{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    if (queue.transactions.count == 0){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:@"Данный продукт ранее не покупался"];
    } else {
        for(SKPaymentTransaction *transaction in queue.transactions){
            if(transaction.transactionState == SKPaymentTransactionStateRestored){
                if ([transaction.payment.productIdentifier isEqualToString:_currentTest.info.key_ios]){
                    [self buyTest];
                    return;
                }
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:@"Данный продукт ранее не покупался"];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing:
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier isEqualToString:_currentTest.info.key_ios]){
                    [self buyTest];
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                }
                break;
            case SKPaymentTransactionStateRestored:
                if ([transaction.payment.productIdentifier isEqualToString:_currentTest.info.key_ios]){
                    [self buyTest];
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                }
                break;
            case SKPaymentTransactionStateFailed:
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            case SKPaymentTransactionStateDeferred:
                break;
        }
    }
}

#pragma mark - Unwind Segue

-(IBAction)prepareForTestUnwind:(UIStoryboardSegue *)segue {
    self.scrollView.hidden = YES;
    [self loadFullTest];
}

@end
