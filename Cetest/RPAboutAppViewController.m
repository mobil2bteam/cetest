//
//  RPAboutAppViewController.m
//  Cetest
//
//  Created by Ruslan on 3/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "RPAboutAppViewController.h"

@interface RPAboutAppViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation RPAboutAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *aboutAppURL;
    if (self.isHelp) {
        self.navigationItem.title = @"Помощь";
        aboutAppURL = @"http://eclass.cetest.ru/sc/help.html";
    } else{
        self.navigationItem.title = @"О проекте";
        aboutAppURL = @"http://eclass.cetest.ru/sc/info.html";
    }
    NSURL *websiteUrl = [NSURL URLWithString:aboutAppURL];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:websiteUrl];
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [self.webView loadRequest:urlRequest];
}

@end
