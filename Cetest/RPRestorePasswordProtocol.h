//
//  RPRestorePasswordProtocol.h
//  Е-класс
//
//  Created by Ruslan on 8/20/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RPRestorePasswordProtocol <NSObject>

- (void)restorePasswordWithEmail:(NSString *)email;

@end
