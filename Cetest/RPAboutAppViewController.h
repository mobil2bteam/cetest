//
//  RPAboutAppViewController.h
//  Cetest
//
//  Created by Ruslan on 3/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPAboutAppViewController : UIViewController

@property (nonatomic, assign) BOOL isHelp;

@end
