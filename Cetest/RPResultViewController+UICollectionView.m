//
//  RPResultViewController+UICollectionView.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPResultViewController+UICollectionView.h"
#import "RPResultHeaderCollectionViewCell.h"
#import "RPResultCell.h"
#import "RPUserInfo.h"

@implementation RPResultViewController (UICollectionView)


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.filteredResults.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.filteredResults[section].count;
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.resultsCollectionView.bounds), 50);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RPResultCell *cell = (RPResultCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([RPResultCell class]) forIndexPath:indexPath];
    cell.resultLabel.text = self.filteredResults[indexPath.section][indexPath.row][0].test_name;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        RPResultHeaderCollectionViewCell *headerView = (RPResultHeaderCollectionViewCell*)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPResultHeaderCollectionViewCell class]) forIndexPath:indexPath];
        headerView.arrowImageView.hidden = YES;
        headerView.headerLabel.text = self.filteredResults[indexPath.section][0][0].class_name;
        reusableview = headerView;
    }
    return reusableview;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 50);
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self showTestWithIndexPath:indexPath];
}

@end
