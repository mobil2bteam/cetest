//
//  RPTestViewController+UITableView.m
//  Е-класс
//
//  Created by Ruslan on 5/3/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPTestViewController+UITableView.h"
#import "RPFullTest.h"
#import "RPThemeTableViewCell.h"
#import "UIViewController+RPShowAlert.h"
#import "Constants.h"

@implementation RPTestViewController (UITableView)

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [self theme_list].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RPThemeTableViewCell *cell = (RPThemeTableViewCell*)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([RPThemeTableViewCell class]) forIndexPath:indexPath];
    cell.themeLabel.text = [self theme_list][indexPath.row].name;
    cell.contentView.backgroundColor = cell.backgroundColor = [UIColor whiteColor];
    
    if ([self theme_list][indexPath.row].sp) {
        if ([self theme_list][indexPath.row].sp >= [self theme_list][indexPath.row].ps) {
            cell.resultLabel.textColor = kSuccessGreenColor;
        } else{
            cell.resultLabel.textColor = [UIColor redColor];
        }
        cell.resultLabel.text = [NSString stringWithFormat:@"%ld", (long)[self theme_list][indexPath.row].sp];
        // если первая ячейка
        if (indexPath.row == 0) {
            if (self.theme_list.count > 1) {
                if (self.theme_list[1].sp) {
                    cell.bottomLineView.hidden = NO;
                }
            }
        } else if (indexPath.row == self.theme_list.count - 1) {
            // если последняя ячейка
            if (self.theme_list[indexPath.row - 1].sp) {
                cell.topLineView.hidden = NO;
            }
        } else {
            // если у ячейки есть соседние ячейки
            if (self.theme_list[indexPath.row - 1].sp) {
                cell.topLineView.hidden = NO;
            }
            if (self.theme_list[indexPath.row + 1].sp) {
                cell.bottomLineView.hidden = NO;
            }
        }
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.currentTest.options.theme_open) {
        [self performSegueWithIdentifier:@"themeOpenSegue" sender:self];
    } else {
        NSString *message = @"Для просмотра купите тест или введите промо код";
        [self showMessage:message];
    }
}

@end
