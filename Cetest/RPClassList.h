//
//  RPClassList.h
//  Cetest
//
//  Created by Ruslan on 3/25/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class RPClass;

@interface RPClassList : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<RPClass *> *class_list;

+ (RPClassList*) sharedClassList ;

+ (NSInteger)priceForClass:(NSString *)className;

+ (NSString *)productKeyForClassWithID:(NSInteger)ID;

@end

@interface RPClass : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger ID;

@property (nonatomic, assign) NSInteger active;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, copy) NSString *key_android;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *key_ios;

@end

