//
//  RPTestBoughtController.h
//  Cetest
//
//  Created by Ruslan on 3/30/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RPFullTest;

@interface RPTestBoughtController : UIViewController

@property (nonatomic, strong) RPFullTest *currentTest;

@property (nonatomic, assign) BOOL isBuyViaPromoCode;

@property (nonatomic, strong) NSString *promoCode;

@end
