//
//  RPResultButton.h
//  Cetest
//
//  Created by Ruslan on 4/14/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RPResultButton : UIButton

@property (nonatomic, strong) NSIndexPath *indexPath;

@end
