//
//  RPResultStep2ViewController.m
//  Е-класс
//
//  Created by Ruslan on 4/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "RPResultStep2ViewController.h"
#import "RPResultCell.h"
#import "RPResultHeaderCollectionViewCell.h"
#import "RPUserInfo.h"

@interface RPResultStep2ViewController ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resultsCollectionViewHeightConstraint;

@end

@implementation RPResultStep2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.filteredTests[0].class_name;

    [self.resultsCollectionView registerNib:[UINib nibWithNibName:@"RPResultHeaderCollectionViewCell" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([RPResultHeaderCollectionViewCell class])];
    
    [self.resultsCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([RPResultCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([RPResultCell class])];
    
    // add observer to automatic update resultsCollectionView's height
    [self addObserver:self forKeyPath:@"resultsCollectionView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [self.view setNeedsDisplay];
    [self.resultsCollectionView reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.resultsCollectionView reloadData];
    });
}

- (void)dealloc{
    @try {
        [self removeObserver:self forKeyPath:@"resultsCollectionView.contentSize"];
    } @catch (NSException *exception) {
        
    } @finally {
        
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:[UIApplication sharedApplication]];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationWillResignActiveNotification
                                                  object:[UIApplication sharedApplication]];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:@"resultsCollectionView.contentSize"]) {
        CGFloat newHeight = self.resultsCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.resultsCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

@end
