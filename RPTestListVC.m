//
//  RPViewController.m
//  Cetest
//
//  Created by Ruslan on 3/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import "Header.h"
#import "RPTestListVC.h"
#import "RPTestCollectionViewCell.h"
#import "RPTestCollectionReusableView.h"
#import "RPTestViewController.h"
#import <StoreKit/StoreKit.h>
#import "RPClassCollectionViewCell.h"
#import "RPClassList.h"
#import "RPSigInViewController.h"

@import FirebaseInstanceID;
@import FirebaseMessaging;

@interface RPTestListVC () <SKProductsRequestDelegate, SKPaymentTransactionObserver, RPSignInProtocol>

@end

@implementation RPTestListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (![APP_DELEGATE.navControllers containsObject:self.navigationController]) {
        [APP_DELEGATE.navControllers addObject:self.navigationController];
    }
    if (![kUserDefaults valueForKey:kSelectedClassKey]) {
        [kUserDefaults setObject:kAllClassedKey forKey:kSelectedClassKey];
        [kUserDefaults synchronize];
    }
    self.selectedClass = [kUserDefaults valueForKey:kSelectedClassKey];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self prepareView];
    [self addRefreshControllToScrollView];
    
    // если это (Мои тесты) и нет ни бесплатных ни купленых тестов, то показываем
    if (self.isUserTests) {
        if (kUser.isLogin) {
            if (!self.buyTestArray.count && !self.freeTestArray.count) {
                self.noUserTestsView.hidden = NO;
            }
        } else {
            self.registrationView.hidden = NO;
        }
    } else {
        if (self.allTestArray.count == 0) {
            self.noTestsView.hidden = NO;
        } else {
            self.noTestsView.hidden = YES;
        }
    }
}

- (void)dealloc{
    [[SKPaymentQueue defaultQueue]removeTransactionObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.classCollectionView.hidden = NO;
    self.allTestArray = APP_DELEGATE.allTestArray;
    self.freeTestArray = APP_DELEGATE.freeTestArray;
    self.buyTestArray = APP_DELEGATE.buyTestArray;
    [self.testCollectionView reloadData];
    if (self.isUserTests) {
        if (kUser.isLogin) {
            self.registrationView.hidden = YES;
            if (!self.buyTestArray.count && !self.freeTestArray.count) {
                self.noUserTestsView.hidden = NO;
                self.classCollectionView.hidden = YES;
            } else {
                self.noUserTestsView.hidden = YES;
            }
        } else {
            self.registrationView.hidden = NO;
            self.classCollectionView.hidden = YES;
        }
    } else {
        if (self.allTestArray.count == 0) {
            self.classCollectionView.hidden = YES;
            self.noTestsView.hidden = NO;
        } else {
            self.noTestsView.hidden = YES;
        }
    }
}


- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [self.testCollectionView performBatchUpdates:^{
    } completion:^(BOOL finished) {
        [self.testCollectionView reloadData];
    }];
}

- (void)reloadCollectionView{
    self.selectedTestArray = nil;
    [self.testCollectionView reloadData];
}

#pragma mark - RPSignInProtocol

- (void)authorization{
    [self.navigationController.tabBarController setSelectedIndex:2];
}

- (void)registration{
    [self.navigationController.tabBarController setSelectedIndex:2];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showTestInfoSegue"]) {
        RPTestViewController *newController = segue.destinationViewController;
        NSIndexPath *selectedIndexPath = [self.testCollectionView indexPathsForSelectedItems][0];
        RPTest *currentTest;
        if (self.isUserTests) {
            if (self.freeTestArray.count) {
                if (!selectedIndexPath.section) {
                    currentTest = self.freeTestArray[selectedIndexPath.row];
                } else {
                    currentTest = self.buyTestArray[selectedIndexPath.section - 1][selectedIndexPath.row];
                }
            } else {
                currentTest = self.buyTestArray[selectedIndexPath.section][selectedIndexPath.row];
            }
        } else {
            currentTest = self.selectedTestArray[selectedIndexPath.section][selectedIndexPath.row];
        }
        newController.test = currentTest;
    }
}

- (IBAction)buyClassButtonPressed:(UIButton *)sender {
    if ([kUser isLogin]) {
        __weak typeof(self) weakSelf = self;
        RPTest *test = self.selectedTestArray[sender.tag][0];
        _selectedTest = test;
        NSString *message = [NSString stringWithFormat:@"Вы действительно хотите купить %@ за %ld руб.", test.class_name, (long)[RPClassList priceForClass:test.class_name]];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Купить класс"
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Купить"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * _Nonnull action) {
                                       [weakSelf prepareBuyClass];
                                   }];
        UIAlertAction *restoreAction = [UIAlertAction
                                        actionWithTitle:@"Я уже покупал"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * _Nonnull action) {
                                            [weakSelf restore];
                                        }];
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Отменить"
                                       style:UIAlertActionStyleCancel
                                       handler:nil];
        [alertController addAction:okAction];
        [alertController addAction:restoreAction];
        [alertController addAction:cancelAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        RPSigInViewController *shareController = [[RPSigInViewController alloc] initWithNibName: @"RPSigInViewController" bundle: nil];
        shareController.delegate = self;
        shareController.modalPresentationStyle = UIModalPresentationOverFullScreen;
        shareController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:shareController animated:YES completion:nil];
    }
}

- (void)prepareBuyClass{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if([SKPaymentQueue canMakePayments]){
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:[RPClassList productKeyForClassWithID:_selectedTest.class_id]]];
        productsRequest.delegate = self;
        [productsRequest start];
    }
    else{
        [self showMessage:@"Покупки недоступны"];
    }
}

- (IBAction)registrationButtonPressed:(id)sender {
    [self.navigationController.tabBarController setSelectedIndex:2];
}

- (void)buyClass{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postBuyClassWithID:_selectedTest.class_id onSuccess:^(RPError *error, RPResult *result) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            [self showMessage:error.msg];
        } else {
            if ([result.result isEqualToString:@"ok"]) {
                if (kUser.isLogin){
                    UIAlertController *alertController = [UIAlertController
                                                          alertControllerWithTitle:nil
                                                          message:@"Класс куплен"
                                                          preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *okAction = [UIAlertAction
                                               actionWithTitle:@"Ok"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * _Nonnull action) {
                                                   [self loadTests];
                                               }];
                    [alertController addAction:okAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:kErrorMessage];
    }];
}

#pragma mark - Methods

- (void)prepareView{
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.2705 green:0.5218 blue:0.9986 alpha:1.0];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]}];
}

- (void)addRefreshControllToScrollView{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor blackColor];
    [self.refreshControl addTarget:self action:@selector(loadTests) forControlEvents:UIControlEventValueChanged];
    [self.testCollectionView addSubview:self.refreshControl];
}

- (BOOL)hasUserBoughtClassForSection:(NSInteger) section{
    for (RPTest *test in self.selectedTestArray[section]) {
        if (!test.buy_id) {
            return NO;
        }
    }
    return YES;
}

- (void)loadClasses{
    if (![RPClassList sharedClassList].class_list.count) {
        __weak typeof(self) weakSelf = self;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[RPServerManager sharedManager]getClassListOnSuccess:^(RPClassList *class_list, RPError *error) {
            if (error) {
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                    [weakSelf loadClasses];
                } andBackHandler:^(UIAlertAction *action) {
                    [RPUser logOut];
                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                }];
            } else {
                [weakSelf loadTests];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
                [weakSelf loadClasses];
            } andBackHandler:^(UIAlertAction *action) {
                [RPUser logOut];
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
            }];
        }];
    } else {
        [self loadTests];
    }
}

- (void)loadTests{
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[RPServerManager sharedManager]postGetAllDataOnSuccess:^(RPError *error) {
        [self.refreshControl endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        if (error) {
            [weakSelf showMessage:error.msg withRepeatHandler:^(UIAlertAction *action) {
                [weakSelf loadTests];
            }];
        } else {
            weakSelf.allTestArray = APP_DELEGATE.allTestArray;
            weakSelf.freeTestArray = APP_DELEGATE.freeTestArray;
            weakSelf.buyTestArray = APP_DELEGATE.buyTestArray;
            weakSelf.selectedTestArray = nil;
            [weakSelf.testCollectionView reloadData];
            if (self.isUserTests) {
                if (kUser.isLogin) {
                    weakSelf.registrationView.hidden = YES;
                    if (!weakSelf.buyTestArray.count && !weakSelf.freeTestArray.count) {
                        weakSelf.noUserTestsView.hidden = NO;
                    } else {
                        weakSelf.noUserTestsView.hidden = YES;
                        weakSelf.classCollectionView.hidden = NO;
                    }
                } else {
                    self.registrationView.hidden = NO;
                }
            } else {
                if (self.allTestArray.count == 0) {
                    self.noTestsView.hidden = NO;
                } else {
                    self.noTestsView.hidden = YES;
                }
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self.refreshControl endRefreshing];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [weakSelf showMessage:kErrorMessage withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf loadTests];
        }];
    }];
}

- (RPTest *)currentTest:(NSIndexPath *)indexPath{
    if (!self.isUserTests) {
        return self.selectedTestArray[indexPath.section][indexPath.row];
    } else {
        if (self.freeTestArray.count) {
            if (!indexPath.section) {
                return self.freeTestArray[indexPath.row];
            } else {
                return self.buyTestArray[indexPath.section - 1][indexPath.row];
            }
        } else {
            return self.buyTestArray[indexPath.section][indexPath.row];
        }
    }
}

- (void)reloadTests{
    self.allTestArray = self.selectedTestArray = self.buyTestArray = nil;
    self.freeTestArray = nil;
    [self.testCollectionView reloadData];
}

- (NSArray <NSArray <RPTest*> *> *)selectedTestArray{
    if (_selectedTestArray) {
        return _selectedTestArray;
    }
    if ([[kUserDefaults valueForKey:kSelectedClassKey] isEqualToString:kAllClassedKey]) {
        self.selectedTestArray = self.allTestArray;
        return self.allTestArray;
    } else {
        for (NSArray <RPTest*> *testArray in self.allTestArray) {
            if ([[kUserDefaults valueForKey:kSelectedClassKey] isEqualToString:testArray[0].class_name]) {
                NSArray <NSArray <RPTest*> *> *selectedTestArray = [NSArray arrayWithObject:testArray];
                self.selectedTestArray = selectedTestArray;
                return selectedTestArray;
            }
        }
        return nil;
    }
}

#pragma mark - In-App

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    NSInteger count = [response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        SKPayment *payment = [SKPayment paymentWithProduct:validProduct];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
    else if(!validProduct){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:@"Нет доступных продуктов"];
    }
}

- (void)restore{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    if (queue.transactions.count == 0){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:@"Данный продукт ранее не покупался"];
    } else {
        for(SKPaymentTransaction *transaction in queue.transactions){
            if(transaction.transactionState == SKPaymentTransactionStateRestored){
                if ([transaction.payment.productIdentifier isEqualToString:[RPClassList productKeyForClassWithID:_selectedTest.class_id]]){
                    [self buyClass];
                    return;
                }
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self showMessage:@"Данный продукт ранее не покупался"];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions{
    for(SKPaymentTransaction *transaction in transactions){
        switch(transaction.transactionState){
            case SKPaymentTransactionStatePurchasing:
                //called when the user is in the process of purchasing, do not add any of your own code here.
                break;
            case SKPaymentTransactionStatePurchased:
                if ([transaction.payment.productIdentifier isEqualToString:[RPClassList productKeyForClassWithID:_selectedTest.class_id]]){
                    [self buyClass];
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                }
                break;
            case SKPaymentTransactionStateRestored:
                break;
            case SKPaymentTransactionStateFailed:
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
        }
    }
}

@end
