//
//  RPViewController.h
//  Cetest
//
//  Created by Ruslan on 3/10/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RPTest;
@class RPClassList;

IB_DESIGNABLE

@interface RPTestListVC : UIViewController

@property (nonatomic, assign) IBInspectable BOOL isUserTests;

@property (weak, nonatomic) IBOutlet UICollectionView *testCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *classCollectionView;

@property (weak, nonatomic) IBOutlet UIView *noTestsView;

@property (weak, nonatomic) IBOutlet UIView *noUserTestsView;

@property (weak, nonatomic) IBOutlet UIView *registrationView;

@property (strong, nonatomic) NSString *selectedClass;

@property (nonatomic, assign) BOOL animate;

@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) RPTest *selectedTest;

@property (nonatomic, strong) RPClassList *classList;

@property (nonatomic, strong) NSArray <NSArray <RPTest *> *> *buyTestArray;

@property (nonatomic, strong) NSArray <RPTest*> *freeTestArray;

@property (nonatomic, strong) NSArray <NSArray <RPTest*> *> *allTestArray;

@property (nonatomic, strong) NSArray <NSArray <RPTest*> *> *selectedTestArray;


- (void)reloadCollectionView;

- (BOOL)hasUserBoughtClassForSection:(NSInteger) section;

@end
